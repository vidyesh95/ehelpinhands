import 'package:ehelpinhands/ui/ManageGroups.dart';
import 'package:ehelpinhands/ui/GroupName.dart';
import 'package:flutter/material.dart';

class AdminSide extends StatefulWidget {
  _AdminSide createState() => _AdminSide();
}

class _AdminSide extends State<AdminSide> with TickerProviderStateMixin {
  TabController tabController1;
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'msc',
      home: new DefaultTabController(
        length: 2,
        child: new Scaffold(
          appBar: new PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: new Container(
              color: Colors.amberAccent,
              child: new SafeArea(
                child: Column(
                  children: <Widget>[
                    new Expanded(child: new Container()),
                    new TabBar(
                      labelColor: Colors.black,
                      indicatorColor: Colors.black,
                      indicatorWeight: 4.0,
                      tabs: [
                        Tab(
                          child: Align(
                            alignment: Alignment.center,
                            child: Text("Manage Groups",
                              style: TextStyle(fontSize: 18),
                            ),
                          ),
                        ),
                        Tab(
                          child: Align(
                            alignment: Alignment.center,
                            child: Text("Requested Groups",
                              style: TextStyle(fontSize: 18),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          body: new TabBarView(
            children: <Widget>[
              ManageGroups(),
              GroupName(),
              /*GroupName(),*/
            ],
          ),
        ),
      ),
    );
  }

  /*GroupName() {
    Navigator.pop(context);
    Navigator.of(context).push(
      new MaterialPageRoute(builder: (context) => new GroupName()),
    );
  }*/
}
