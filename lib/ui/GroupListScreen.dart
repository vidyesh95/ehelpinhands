import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/*+++++++++++++++++++++3.2++++++++++++++++++++*/
class GroupListScreen extends StatefulWidget {
  bool showall;
  GroupListScreen(this.showall);
  @override
  State<StatefulWidget> createState() {
    return GroupListScreenState();
  }
}

class GroupListScreenState extends State<GroupListScreen> {
  TextEditingController search = new TextEditingController();
  var list = [
    " ",
    "Public",
    "1Community Name will be here in...",
    "2Community Name will be here in...",
    "3Community Name will be here in...",
  ];
  var _searchProduct = [];
  @override
  void initState() {
    super.initState();
    if (!widget.showall) list.removeAt(0);
    onSearchTextChanged("");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: util.primaryYello,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 10,
            ),
            Row(
              children: <Widget>[
                InkWell(
                    onTap: () => Navigator.pop(context),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(24, 0, 76, 40),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Icon(Icons.arrow_back),
                      ),
                    )),
                Container(
                  padding: EdgeInsets.fromLTRB(26, 45, 56, 0),
                  child: Column(children: <Widget>[
                    Center(
                        child: SizedBox(
                          height: 70,
                          width: 70,
                          child: Image.asset("assets/images/ehh_logo_d.png"),
                    )),
                    SizedBox(
                      height: 10,
                    ),
                  ]),
                )
              ],
            ),
            Expanded(
              child: Container(
                color: util.primaryBrown,
                padding: EdgeInsets.fromLTRB(0, 50, 0, 10),
                child: ListView.separated(
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.pop(
                              context, {"selected": _searchProduct[index]});
                        },
                        child: Container(
                          color: Colors.amberAccent,
                          padding: EdgeInsets.all(10),
                          child: Row(
                            children: <Widget>[
                              SizedBox(
                                  width: 18,
                                  height: 18,
                                  child: Image.asset(
                                      "assets/images/ehh_logo_d.png")),
                              VerticalDivider(
                                width: 10,
                              ),
                              Text(
                                _searchProduct[index],
                                style: TextStyle(
                                    fontSize: 20, color: util.primaryBrown),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Container(
                        color: util.primaryBrown,
                        height: 1,
                      );
                    },
                    itemCount: _searchProduct.length),
              ),
            )
          ],
        ),
      ),
    );
  }

  onSearchTextChanged(String text) async {
    print("changed -$text-");
    _searchProduct.clear();
    if (text.isEmpty) {
      _searchProduct.addAll(list);
      setState(() {});
      return;
    }

    list.forEach((item) {
      if (item.toLowerCase().contains(text.toLowerCase()) ||
          item.toLowerCase() == text.toLowerCase()) {
        _searchProduct.add(item);
        print("matched $item  --- $text");
      }
    });

    setState(() {});
  }
}
