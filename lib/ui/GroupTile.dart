import 'package:ehelpinhands/ui/ProfileScreen.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GroupTile extends StatefulWidget {
  GroupModel item;
  int index;
  Function onClick;

  GroupTile(this.item,this.index,this.onClick);

  @override
  _GroupTileState createState() => _GroupTileState();
}

class _GroupTileState extends State<GroupTile> {
//  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          widget.item.isSelected = !widget.item.isSelected;
          widget.onClick(widget.item.isSelected);
        });
      },
      child: Container(
        color: widget.item.isSelected ? util.primaryYello : Colors.white,
        padding: EdgeInsets.all(10),
        width: double.maxFinite,
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                SizedBox(
                    height: 40,
                    width: 40,
                    child: Image.asset(
                      "assets/images/ehh_logo_d.png",
                    )),
                SizedBox(width: 10,),
                Expanded(
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(widget.item.gName+" ${widget.index}",style: TextStyle(fontSize: 18),),
                      SizedBox(height: 10,),
                      Row(
                        children: <Widget>[
                          SizedBox(
                              height: 15,
                              width: 15,
                              child: Image.asset(
                                "assets/images/ehh_logo_d.png",
                              )),
                          SizedBox(width: 10,),
                          Text(widget.item.adminName,style: TextStyle(fontSize: 16),)
                        ],
                      )
                    ],
                  ),
                ),
                if(widget.item.isSelected)
                SizedBox(
                  width: 30,
                  height: 30,
                  child: Image.asset(
                    "assets/images/ic_checked_brown.png",
                    color: util.primaryBrown,
                  ),
                ),SizedBox(width: 5,)
              ],
            ),
          ],
        ),
      ),
    );
  }
}
