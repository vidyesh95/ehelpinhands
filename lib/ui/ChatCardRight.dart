import 'package:ehelpinhands/models/CommentData.dart';
import 'package:ehelpinhands/ui/CommentScreen.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatCardRight extends StatelessWidget {
  CommentData item;
  ChatCardRight(this.item);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding:EdgeInsets.fromLTRB(80, 5,5, 5) ,

      child: Align(alignment: Alignment.topCenter,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Align(
                          alignment: Alignment.centerRight,
                          child: Text("You ",style: TextStyle(fontSize: 14
                              ,fontWeight: FontWeight.bold,color: util.primaryYello),)),
                      SizedBox(height: 3,),
                      Card( shape: RoundedRectangleBorder(

                    borderRadius: BorderRadius.circular(10),
                  ),color: Colors.white,child: Container(padding:EdgeInsets.fromLTRB(10,5,10,5  ),child: Text(item.message,style: TextStyle(fontSize: 20 ))))
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(width: 5,),
//            Container( padding: const EdgeInsets.all(0.0),child: IconButton(iconSize:20,icon: Icon(Icons.supervised_user_circle)))
          ],
        ),
      ),
    );
  }
}
