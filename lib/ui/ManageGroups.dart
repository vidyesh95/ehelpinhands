import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ManageGroups extends StatefulWidget {
  _ManageGroups createState() => _ManageGroups();
}

class _ManageGroups extends State<ManageGroups> with TickerProviderStateMixin {
  TabController tabController;
  var allcount = "";
  var communitiescount = "";
  var commercialscount = "";
  var govt_offilcialscount = "";
  var individualscount = "";
  var placescount = "";

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new DefaultTabController(
        length: 6,
        child: new Scaffold(
          backgroundColor: util.primaryBrown,
          body: Column(
            children: <Widget>[
              Container(
                color: Colors.white,
                child: TabBar(
                  labelColor: Colors.black,
                  indicatorColor: Colors.yellow,
                  unselectedLabelColor: Colors.black45,
                  labelStyle: TextStyle(fontWeight: FontWeight.bold),
                  indicatorWeight: 3,
                  isScrollable: true,
                  tabs: [
                    Container(
                      height: 35,
                      child: new Tab(
                        child: Text("All $allcount"),
                      ),
                    ),
                    Container(
                      height: 35,
                      child: new Tab(
                        child: Text("Community $communitiescount"),
                      ),
                    ),
                    Container(
                      height: 35,
                      child: new Tab(
                        child: Text(
                          "Commercials $commercialscount",
                        ),
                      ),
                    ),
                    Container(
                      height: 35,
                      child: new Tab(
                        child: Text(
                          "Govt. & Officials $govt_offilcialscount",
                        ),
                      ),
                    ),
                    Container(
                      height: 35,
                      child: new Tab(
                        child: Text(
                          "Individuals $individualscount",
                        ),
                      ),
                    ),
                    Container(
                      height: 35,
                      child: new Tab(
                        child: Text(
                          "Places $placescount",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                color: Colors.brown,
                height: 2,
              ),
              Container(
                  color: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.search),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: TextField(
                            style: TextStyle(fontSize: 20),
                            decoration: InputDecoration(
                                hintStyle: TextStyle(color: util.lightGrey),
                                border: InputBorder.none,
                                hintText: "Search..."),
                          ),
                        ),
                      ],
                    ),
                  )),
              Container(
                color: Colors.black,
                height: 12,
              ),
              Container(
                alignment: Alignment.topLeft,
                height: 65,
                child: ListGroups(),
              ),
              Container(
                color: Colors.black,
                height: 12,
              ),
              Container(
                alignment: Alignment.topLeft,
                height: 65,
                child: ListGroups(),
              ),
              Container(
                color: Colors.black,
                height: 12,
              ),
              Container(
                alignment: Alignment.topLeft,
                height: 65,
                child: ListGroups(),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ListGroups extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget list = new Container(
      height: 30,
      child: new Row(children: [
        new Image.asset("assets/images/ehh_logo_d.png", height: 30, width: 30),
        new Expanded(
          child: new Column(
            children: [
              new Expanded(
                child: new Row(children: [
                  new Padding(
                    padding: EdgeInsets.all(10.0),
                    child: new Text(
                      "Community Name will come here...",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  )
                ]),
              ),
              new Expanded(
                  child: new Row(children: [
                new Image.asset("assets/images/ehh_logo_d.png",
                    height: 20, width: 20),
                new Text(
                  "Community's Creator Name",
                  style: TextStyle(fontSize: 13),
                ),
              ]))
            ],
          ),
        )
      ]),
    );
    return new Scaffold(
      body: new ListView(
        children: [list],
      ),
    );
  }
}
