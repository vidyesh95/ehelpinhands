import 'package:ehelpinhands/data/FlutterSharedPrefrence.dart';
import 'package:ehelpinhands/repository/UserRepository.dart';
import 'package:ehelpinhands/ui/GroupTile.dart';
import 'package:ehelpinhands/ui/home_page.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}
var all=[
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
];
var communities=[
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
];
var commercial=[
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
];
var govt=[
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
];
var personal=[
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
];
var place=[
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
  GroupModel("Group","abc "),
];

class _ProfileScreenState extends State<ProfileScreen> with TickerProviderStateMixin {
  ProgressDialog pr;
  TabController defaultTabController;
  int selectedCount=0;
  bool isPublic ;
  var _allTabCount = "";
  var commonContentPadding = EdgeInsets.fromLTRB(10, 0, 10, 0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: util.primaryBrown,
      body: Builder(
        builder: (context) => SafeArea(
            child: Column(
          children: <Widget>[
            Container(
                color: util.primaryYello,
                padding: EdgeInsets.all(15),
                child: Row(
                  children: <Widget>[
                    SizedBox(
                        height: 35,
                        width: 35,
                        child: Image.asset(
                          "assets/images/ehh_logo_d.png",
                        )),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                        child: Text(
                      "Select Groups to Send Request",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    )),
                    SizedBox(
                      width: 55,
                    )
                  ],
                )),
            Container(
              color: Colors.white,
              child: TabBar(
                controller: defaultTabController,
                indicatorWeight: 3,
                labelColor: Colors.black,
                labelStyle: TextStyle(fontWeight: FontWeight.bold),
                unselectedLabelColor: Colors.black45,
                indicatorColor: Colors.yellow,
                isScrollable: true,
                tabs: [
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text(
                        "Public $_allTabCount",
                      ),
                    ),
                  ),
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text(
                        "All $_allTabCount",
                      ),
                    ),
                  ),
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text(
                        "Communities $_allTabCount",
//                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ),
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text(
                        "Commercial $_allTabCount",
                      ),
                    ),
                  ),
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text("Govt. & Officials $_allTabCount"),
                    ),
                  ),
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text("Personal / Individual $_allTabCount"),
                    ),
                  ),
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text("Place $_allTabCount"),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.brown,
              height: 2,
            ),
            Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.search),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: TextField(
                          onChanged: onSearchTextChanged,
                          style: TextStyle(fontSize: 20),
                          decoration: InputDecoration(
                              hintStyle: TextStyle(color: util.lightGrey),
                              border: InputBorder.none,
                              hintText: "Search..."),
                        ),
                      ),
                    ],
                  ),
                )),
            SizedBox(height: 10,),
            Expanded(
                child: TabBarView(
              controller: defaultTabController,
              children: <Widget>[
                Column(mainAxisSize: MainAxisSize.max,mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Column(mainAxisSize: MainAxisSize.max,mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          InkWell(onTap: () {
                            setState(() {
                              isPublic=true;
                            });
                          },
                            child: Container(
                              child: Column(
                                children: <Widget>[

                                  Stack(alignment: Alignment.topRight,
                                    children: <Widget>[
                                      Container(
                                          margin: EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
                                          width: 70,
                                          height: 70,
                                          child: Image.asset(
                                            "assets/images/ic_user_w.png",
                                            color: isPublic!=null?isPublic ? util.primaryYello : Colors.white:Colors.white,
                                          )),
                                      if (isPublic!=null&&isPublic)
                                        SizedBox(
                                          width: 18,
                                          height: 18,
                                          child: Image.asset(
                                            "assets/images/ic_checked_brown.png",
                                            color: util.primaryYello,
                                          ),
                                        ),
                                    ],
                                  ),

                                  Text(
                                    "Public Profile",
                                    style: TextStyle(color: isPublic!=null?isPublic ? util.primaryYello : Colors.white:Colors.white, fontSize: 24),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Your Profile & Your Posts would be Public...\n(Recommended)",
                                    style: TextStyle(color:isPublic!=null?isPublic ? util.primaryYello : Colors.white:Colors.white),
                                    textAlign: TextAlign.center,
                                  )
                                ],
                              ),
                            ),
                          ),SizedBox(height: 60,),
                          InkWell(onTap: () {
                            setState(() {
                              isPublic=false;
                            });
                          },
                            child: Container(
                              child: Column(
                                children: <Widget>[
                                  Stack(alignment: Alignment.topRight,
                                    children: <Widget>[
                                      Container(
                                          margin: EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
                                          width: 70,
                                          height: 70,
                                          child: Image.asset(
                                            "assets/images/eHH_Logo_Db.png",
                                            color: isPublic!=null?!isPublic? util.primaryYello:Colors.white:Colors.white,
                                          )),   if (isPublic!=null&&!isPublic)
                                        SizedBox(
                                          width: 18,
                                          height: 18,
                                          child: Image.asset(
                                            "assets/images/ic_checked_brown.png",
                                            color: util.primaryYello,
                                          ),
                                        ),
                                    ],
                                  ),

                                  Text(
                                    "Anonymous Profile",
                                    style: TextStyle(color: isPublic!=null?!isPublic? util.primaryYello:Colors.white:Colors.white, fontSize: 24),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Your Profile & Your Posts would remain Anonymous...",
                                    style: TextStyle(color:isPublic!=null?!isPublic? util.primaryYello:Colors.white:Colors.white),
                                    textAlign: TextAlign.center,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],

                      ),
                    ),
                  ],
                ),

                ListView.separated(separatorBuilder:(context, index) =>  SizedBox(height: 10,),itemBuilder: (context, index) => GroupTile(all[index],index,(value){
                  if(value){
                  setState(() {
                    selectedCount=selectedCount+1;
                  });
                  }else{
                    setState(() {
                      selectedCount=selectedCount-1;
                    });

                  }
                }),itemCount: all.length,),
                ListView.separated(separatorBuilder:(context, index) =>  SizedBox(height: 10,),itemBuilder: (context, index) => GroupTile(communities[index],index,(value){
                  if(value){
                    setState(() {
                      selectedCount=selectedCount+1;
                    });
                  }else{
                    setState(() {
                      selectedCount=selectedCount-1;
                    });

                  }
                }),itemCount: communities.length,),
                ListView.separated(separatorBuilder:(context, index) =>  SizedBox(height: 10,),itemBuilder: (context, index) => GroupTile(commercial[index],index,(value){
                  if(value){
                    setState(() {
                      selectedCount=selectedCount+1;
                    });
                  }else{
                    setState(() {
                      selectedCount=selectedCount-1;
                    });

                  }
                }),itemCount: commercial.length,),
                ListView.separated(separatorBuilder:(context, index) =>  SizedBox(height: 10,),itemBuilder: (context, index) => GroupTile(govt[index],index,(value){
                  if(value){
                    setState(() {
                      selectedCount=selectedCount+1;
                    });
                  }else{
                    setState(() {
                      selectedCount=selectedCount-1;
                    });

                  }
                }),itemCount: govt.length,),
                ListView.separated(separatorBuilder:(context, index) =>  SizedBox(height: 10,),itemBuilder: (context, index) => GroupTile(personal[index],index,(value){
                  if(value){
                    setState(() {
                      selectedCount=selectedCount+1;
                    });
                  }else{
                    setState(() {
                      selectedCount=selectedCount-1;
                    });

                  }
                }),itemCount: personal.length,),
                ListView.separated(separatorBuilder:(context, index) =>  SizedBox(height: 10,),itemBuilder: (context, index) => GroupTile(place[index],index,(value){
                  if(value){
                    setState(() {
                      selectedCount=selectedCount+1;
                    });
                  }else{
                    setState(() {
                      selectedCount=selectedCount-1;
                    });

                  }
                }),itemCount: place.length,)
              ],

            )),
            if(defaultTabController.index==0)
              InkWell(onTap: (){
                if(isPublic==null){
                  Scaffold.of(context).showSnackBar(SnackBar(
                    backgroundColor: util.urgentRed,
                    content: Text("Select atleast 1 from above"),duration: Duration(seconds: 1),
                  ));
                  return;
                }
                defaultTabController.index=1;
              },child: Container(width:double.maxFinite,alignment:Alignment.center,color:util.primaryYello,padding:EdgeInsets.all(25),child: Text("${isPublic!=null?isPublic?"Public Selected & Continue...":"Anonymous Selected & Continue...":" Select & Continue..."} ",style: TextStyle(fontSize: 22),),))
            else
              Container(width:double.maxFinite,alignment:Alignment.center,color:util.primaryYello,child: Row(
                children: <Widget>[  InkWell(onTap: (){
                  Navigator.of(context).pushAndRemoveUntil(
                    new MaterialPageRoute(builder: (context) => new HomePage()),
                        (Route<dynamic> route) => false,
                  );
                },child: Padding(
                  padding: const EdgeInsets.all(25),
                  child: Text("Skip Now",style: TextStyle(fontSize: 22),),
                )),Container(width: 0.5,height: 40,color: util.primaryBrown,),

                InkWell(onTap: (){
                  Navigator.of(context).pushAndRemoveUntil(
                    new MaterialPageRoute(builder: (context) => new HomePage()),
                        (Route<dynamic> route) => false,
                  );
                },child: Padding(padding: EdgeInsets.all(25),child: Expanded(child: Center(child: Text("$selectedCount Selected & Continue...",textAlign: TextAlign.center,style: TextStyle(fontSize: 22),))))),
                ],
              ),)


          ],
        )
//      child: Center(child: Container( child:_signInButton(),))
            ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    pr = new ProgressDialog(context);
    defaultTabController = TabController(vsync: this, length: 7);
    defaultTabController.addListener(() {
      setState(() {

      });
//      loadCategories();
    });
  }


  Widget getGroupTile(GroupModel item){
    bool isSelected=false;

  }
  onSearchTextChanged(String text) async {
//    print("changed -$text-");
//    searchString = text;
//    searchData.clear();
//    if (text.isEmpty) {
//      searchData.addAll(currentTabData);
//      setState(() {});
//      return;
//    }
//
//    currentTabData.forEach((item) {
//      if (item.title.toLowerCase().contains(text.toLowerCase()) || item.firstName.toLowerCase() == text.toLowerCase()) {
//        searchData.add(item);
//        print("matched $item  --- $text");
//      }
//    });
//
//    setState(() {});
  }
}

class GroupModel{
String gName;
String adminName;
bool isSelected=false;
GroupModel(this.gName,this.adminName);
}
