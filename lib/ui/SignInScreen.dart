import 'package:ehelpinhands/data/FlutterSharedPrefrence.dart';
import 'package:ehelpinhands/networking/ApiProvider.dart';
import 'package:ehelpinhands/networking/sign_in.dart';
import 'package:ehelpinhands/repository/UserRepository.dart';
import 'package:ehelpinhands/ui/SignInSignUpScreen.dart';
import 'package:ehelpinhands/ui/home_page.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  ProgressDialog pr;
  var textBoxHeight = 50.0;
  var commonContentPadding = EdgeInsets.fromLTRB(10, 0, 10, 0);

  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pr = new ProgressDialog(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: util.primaryYello,
      body: Builder(
        builder: (context) => SafeArea(
          child: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(50, 30, 50, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 70,
                  width: 70,
                  child: Image.asset("assets/images/ehh_logo_d.png"),
                ),
                SizedBox(
                  height: 40,
                ),
                SizedBox(
                  child: TextFormField(
                      controller: usernameController,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                          filled: true,
                          hintStyle:
                              TextStyle(fontSize: 20, color: util.lightGrey),
                          hintText: "Mobile  No. / Email ID",
                          fillColor: Colors.white,
                          prefixIcon: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              SizedBox(
                                width: 15,
                              ),
                              Icon(
                                Icons.person_outline,
                                color: util.lightGrey,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                width: 1,
                                height: 25,
                                color: util.lightGrey,
                              ),
                              SizedBox(
                                width: 12,
                              )
                            ],
                          ),
                          contentPadding: commonContentPadding,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide.none,
                          ))),
                ),
                SizedBox(
                  height: 35,
                ),
                SizedBox(
                  child: TextFormField(
                      textCapitalization: TextCapitalization.sentences,
                      controller: passwordController,
                      obscureText: true,
                      keyboardType: TextInputType.text,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                          filled: true,
                          hintStyle: TextStyle(
                            fontSize: 20,
                            color: util.lightGrey,
                          ),
                          hintText: "Password",
                          fillColor: Colors.white,
                          prefixIcon: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              SizedBox(
                                width: 15,
                              ),
                              Icon(
                                Icons.lock_outline,
                                color: util.lightGrey,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                width: 1,
                                height: 25,
                                color: util.lightGrey,
                              ),
                              SizedBox(
                                width: 12,
                              )
                            ],
                          ),
                          contentPadding: commonContentPadding,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide: BorderSide.none,
                          ))),
                ),
//                    SizedBox(
//                      height: 12,
//                    ),
//                    Container(
//                        padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
//                        child: Align(
//                            alignment: Alignment.centerRight,
//                            child: InkWell(
//                              child: Text(
//                                "Forget *** ???",
//                                style: TextStyle(color: Colors.white,
//                                    fontSize: 15),
//                              ),
//                            ))),
                SizedBox(
                  height: 50,
                ),
                ButtonTheme(
                  padding: EdgeInsets.all(8),
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(125.0),
                  ),
                  minWidth: double.infinity,
                  child: RaisedButton(
                    onPressed: () async {
                      if (usernameController.text.length != 10 &&
                          !usernameController.text.contains("@")) {
                        showSnackBar("Invalid Mobile No. / Email ID", context);
                      } else if (passwordController.text.length < 8) {
                        showSnackBar("Invalid Password", context);
                      } else {
                        pr.show().then((value) {});
                        var data = {
                          "mobile": usernameController.text,
                          "password": passwordController.text,
                          "fcm_id": await FlutterSharedPrefrence().getFcmId()
                        };
                        UserRepository().signIn(data).then((value) async {
                          pr.hide().then((isHidden) {
                            print(isHidden);
                            if (value.status) {
                              var id = value.data.id;
                              var first_name = value.data.firstName;
                              var last_name = value.data.lastName;
                              var email = value.data.email;
                              var phone_number = value.data.phoneNumber;
                              var is_blocked = value.data.isBlocked;
                              FlutterSharedPrefrence().setUserData(
                                  id.toString(),
                                  first_name,
                                  last_name,
                                  email,
                                  phone_number,
                                  "",
                                  "",
                                  "",
                                  value.data.gender,
                                  value.data.dob,
                                  is_blocked);
                              if (is_blocked) {
                                Fluttertoast.showToast(
                                    msg: "You have been block by the admin!!!");
                                return;
                              }
                              Navigator.of(context).pushAndRemoveUntil(
                                  MaterialPageRoute(
                                builder: (context) {
                                  return HomePage();
                                },
                              ), (Route<dynamic> route) => false);
                            } else {
                              showSnackBar(value.message, context);
                            }
                          });
                        });
                      }
                    },
                    color: Colors.white,
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Text(
                          "Sign In",
                          style: TextStyle(
                            fontSize: 26,
                            fontWeight: FontWeight.bold,
                            color: util.primaryBrown,
                          ),
                        ),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Icon(
                              Icons.arrow_forward_ios,
                              size: 25,
                            ))
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 55,
                ),
                Text(
                  "- - -  New User Sign Up here  - - -",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 30,
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (context) => new SignInSignUpScreen()));
                  },
                  child: Container(
                    child: Stack(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            ButtonTheme(
                              padding: EdgeInsets.fromLTRB(0, 7, 30, 7),
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.only(
                                    topLeft: Radius.circular(125),
                                    bottomLeft: Radius.circular(125)),
                              ),
                              child: Expanded(
                                child: RaisedButton(
                                  disabledColor: Color(0xFFFFF6CF),
                                  color: Color(0xFFFFF6CF),
                                  child: Text(
                                    "Sign Up",
                                    style: TextStyle(
                                      fontSize: 24,
                                      color: util.primaryBrown,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            ButtonTheme(
                              padding: EdgeInsets.fromLTRB(40, 7, 0, 7),
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.only(
                                    topRight: Radius.circular(125),
                                    bottomRight: Radius.circular(125)),
                              ),
                              child: Expanded(
                                child: RaisedButton(
                                  disabledColor: Color(0xFFFFF6CF),
                                  color: Color(0xFFFFF6CF),
                                  onPressed: () {},
                                  child: Text(
                                    " New  ",
                                    style: TextStyle(
                                      fontSize: 24,
                                      color: util.primaryBrown,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          height: 85,
                          width: 85,
                          child: Center(
                            child: Icon(
                              Icons.person_add,
                              size: 60,
                            ),
                          ),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Color(0xFFFFF6CF)),
                        ),
                      ],
                      alignment: Alignment.center,
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "-  or  -",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 20,
                ),
                InkWell(
                  onTap: _signInButton,
                  child: Container(
                    child: Stack(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: ButtonTheme(
                                padding: EdgeInsets.fromLTRB(0, 7, 30, 7),
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topLeft: Radius.circular(125),
                                      bottomLeft: Radius.circular(125)),
                                ),
                                child: RaisedButton(
                                  disabledColor: Color(0xFFFFF6CF),
                                  color: Color(0xFFFFF6CF),
                                  child: Stack(
                                    alignment: Alignment.centerLeft,
                                    children: <Widget>[
                                      Text(
                                        "Sign Up",
                                        style: TextStyle(
                                          fontSize: 24,
                                          color: util.primaryBrown,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: ButtonTheme(
                                padding: EdgeInsets.fromLTRB(40, 7, 0, 7),
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topRight: Radius.circular(125),
                                      bottomRight: Radius.circular(125)),
                                ),
                                child: RaisedButton(
                                  disabledColor: Color(0xFFFFF6CF),
                                  color: Color(0xFFFFF6CF),
                                  child: Stack(
                                    alignment: Alignment.centerRight,
                                    children: <Widget>[
                                      Text(
                                        "Google",
                                        style: TextStyle(
                                          fontSize: 24,
                                          color: util.primaryBrown,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          height: 85,
                          width: 85,
                          child: Center(
                            child: Text(
                              "G",
                              style: TextStyle(fontSize: 80),
                            ),
                          ),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Color(0xFFFFF6CF)),
                        ),
                      ],
                      alignment: Alignment.center,
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Text(
                  "Copyright © eHelpinHands.com All Rights Reserved. Powered by: Infornation Techserve Pvt. Ltd. All the Brands, Trademarks & Copyrights are the Property of their Respective Holders.",
                  style: TextStyle(
                    fontSize: 8,
                  ),
                  textAlign: TextAlign.center,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  showSnackBar(String text, BuildContext context) {
    Scaffold.of(context).showSnackBar(SnackBar(
      backgroundColor: util.urgentRed,
      content: Text(text),
    ));
  }

  _signInButton() {
    signInWithGoogle().then((value) async {
      var _response = ApiProvider.my_response(value);
      var id = _response["data"]['id'];
      var first_name = _response["data"]['first_name'];
      var last_name = _response["data"]['last_name'];
      var email = _response["data"]['email'];
      var phone_number = _response["data"]['phone_number'];
      var device_name = _response["data"]['device_name'];
      var lat = _response["data"]['lat'];
      var lng = _response["data"]['lng'];
      var gender = _response["data"]['gender'];
      var dob = _response["data"]['dob'];
      var is_blocked = _response["data"]['is_blocked'] as bool;
      ProgressDialog pr = new ProgressDialog(context);
      pr.show();
      pr.hide().then((value) {
        FlutterSharedPrefrence().setUserData(
            id.toString(),
            first_name,
            last_name,
            email,
            phone_number,
            device_name,
            lat,
            lng,
            gender,
            dob,
            is_blocked);
        if (is_blocked) {
          Fluttertoast.showToast(msg: "You have been block by the admin!!!");
          return;
        }
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
          builder: (context) {
            return HomePage();
          },
        ), (Route<dynamic> route) => false);
      });
    });
  }
}
