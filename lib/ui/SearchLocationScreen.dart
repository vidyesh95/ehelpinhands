import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:search_map_place/search_map_place.dart';

class SearchLocationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SearchLocationScreenState();
  }
}

class SearchLocationScreenState extends State<SearchLocationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
            padding: EdgeInsets.all(20),
            height: MediaQuery.of(context).size.height,
            child: SearchMapPlaceWidget(
              apiKey: "AIzaSyAebYU52GxjdNq7L9geRWxkZ2kaVGpqvWk",
              // The language of the autocompletion
              language: 'en',

              onSelected: (Place place) async {
                print(place.description);
                Navigator.pop(context, {'selection': place.description});
                final geolocation = await place.geolocation;

                // Will animate the GoogleMap camera, taking us to the selected position with an appropriate zoom
//                          final GoogleMapController controller = await _mapController.future;
//                          controller.animateCamera(CameraUpdate.newLatLng(geolocation.coordinates));
//                          controller.animateCamera(CameraUpdate.newLatLngBounds(geolocation.bounds, 0));
              },
            ),
          ),
        ),
      ),
    );
  }
}
