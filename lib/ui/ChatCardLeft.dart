import 'package:ehelpinhands/models/CommentData.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

  class ChatCardLeft extends StatelessWidget {
      CommentData item;
      ChatCardLeft(this.item);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding:EdgeInsets.fromLTRB(5, 5,80, 5) ,
      child: Align(alignment: Alignment.topCenter,
        child: Row(
          children: <Widget>[

            Expanded(
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Text( " "+item.firstName+" "+item.lastName,style: TextStyle(fontSize: 14 ,fontWeight: FontWeight.bold,color: Colors.white),)),
                      SizedBox(height: 3,),
                      Card( shape: RoundedRectangleBorder(

                        borderRadius: BorderRadius.circular(10),
                      ),color: util.primaryYello,child: Container(padding:EdgeInsets.fromLTRB(10,5,10,5  ),child: Text(item.message,style: TextStyle(fontSize: 20 ))))
                    ],
                  ),
                ),
              ),
            ),
            ],
        ),
      ),
    );
  }
}