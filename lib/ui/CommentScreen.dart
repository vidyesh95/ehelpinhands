import 'dart:async';

import 'package:ehelpinhands/blocs/CommentsBloc.dart';
import 'package:ehelpinhands/data/FlutterSharedPrefrence.dart';
import 'package:ehelpinhands/main.dart';
import 'package:ehelpinhands/models/NeedCommentsModel.dart';
import 'package:ehelpinhands/models/NeedData.dart';
import 'package:ehelpinhands/models/NeedRequestModel.dart';
import 'package:ehelpinhands/networking/Response.dart';
import 'package:ehelpinhands/ui/ChatCardLeft.dart';
import 'package:ehelpinhands/ui/ChatCardRight.dart';
import 'package:ehelpinhands/ui/MainCard.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class CommentScreen extends StatefulWidget {
//  List<CommentData> list = <CommentData>[
//    CommentData(true, "RAj", "How are you"),
//    CommentData(false, "RAj", "How are you"),
//    CommentData(true, "RAj", "How are you"),
//    CommentData(true, "RAj", "How are you"),
//    CommentData(true, "RAj", "How are you"),
//    CommentData(true, "RAj", "How are you"),
//    CommentData(true, "RAj", "How are you"),
//    CommentData(true, "RAj", "How are you"),
//    CommentData(true, "RAj", "How are you"),
//    CommentData(true, "RAj", "How are you"),
//  ];

  NeedData data;
  String type;

  CommentScreen(this.data, this.type);

  @override
  State<StatefulWidget> createState() {
    return CommentScreenState();
  }
}

class CommentScreenState extends State<CommentScreen> {
  String userId;
  CommentBloc _bloc;
  final myController = TextEditingController();
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    FlutterSharedPrefrence().getUserId().then((value) {
      userId = value;
    });
    _bloc = CommentBloc(widget.data.id.toString(), widget.type);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: util.primaryBrown,
      body: SafeArea(
        child: Builder(builder: (context) =>  Column(
            children: <Widget>[
              Container(
                color: Theme.of(context).primaryColor,
                padding: EdgeInsets.all(15),
                child: Row(
                  children: <Widget>[
                    InkWell(
                      child: Icon(Icons.arrow_back_ios),
                      onTap: () => Navigator.pop(context),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Row(
                              children: <Widget>[
                                Text(
                                  "Comments",
                                  style: TextStyle(fontSize: 25),
                                )
                              ],
                            ))),
//                  Icon(Icons.star),
//                  SizedBox(
//                    width: 20,
//                  ),
                    InkWell(
                        onTap: () {
                          if (widget.data.isAnonymous)
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content:  Text("Sorry This ${widget.type=="REPORT"?"Report":(widget.type=="SUGGESTION"?"Suggestion":"Need")} is Anonymous..."),
                            ));
                          else
                            launch("tel://${widget.data.phoneNumber}");
                        },
                        child: Icon(
                          Icons.call,
                          color: util.primaryBrown,
                        )),
//                  Icon(Icons.call),
                  ],
                ),
              ),
              MainCard(widget.data, false, widget.type, userId != null),
              Container(
                height: 2,
                color: Theme.of(context).primaryColor,
              ),
              Expanded(
                child: StreamBuilder<Response<NeedCommentsModel>>(
                    stream: _bloc.allCommentsDataStream,
                    builder: (context, snapshot) {
                      Timer(
                          Duration(milliseconds: 100),
                          () => _scrollController.animateTo(
                                _scrollController.position.maxScrollExtent,
                                curve: Curves.easeOut,
                                duration: const Duration(milliseconds: 300),
                              ));
                      return snapshot.hasData && snapshot.data.data?.data != null
                          ? ListView.separated(
                              controller: _scrollController,
                              itemCount: snapshot.data.data.data.length,
                              separatorBuilder: (BuildContext context, int index) => const Divider(
                                    height: 2,
                                  ),
                              itemBuilder: (context, index) {
                                return snapshot.data.data.data[index].commentById.toString() != userId
                                    ? ChatCardLeft(snapshot.data.data.data[index])
                                    : ChatCardRight(snapshot.data.data.data[index]);
                              })
                          : Center(
                              child: Container(
                                child: Text(
                                  "Comments...",
                                  style: TextStyle(color: util.lightGrey, fontSize: 25),
                                ),
                              ),
                            );
                    }),
              ),
              Container(
                color: Colors.white,
                padding: EdgeInsets.fromLTRB(15, 5, 10, 5),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: TextFormField(
                          controller: myController,
                          style: TextStyle(backgroundColor: Colors.white),
                          decoration: InputDecoration(
                              filled: true,
                              hintText: "Type here",
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                gapPadding: 0,
//                        borderRadius: BorderRadius.circular(25.0),
                              ))),
                    ),
                    VerticalDivider(
                      width: 10,
                    ),
                    InkWell(
                        onTap: () {
                          print("message " + myController.text);
                          _bloc.postComment(myController.text);
                          myController.text = "";
                        },
                        child: SizedBox(height: 40, width: 40, child: Icon(Icons.send)))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

//class CommentData {
//  bool isMine;
//  String username;
//  String message;
//
//  CommentData(this.isMine, this.username, this.message);
//}
