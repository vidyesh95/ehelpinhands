import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoryListScreen extends StatefulWidget{
  bool showall;
  CategoryListScreen(this.showall);
  @override
  State<StatefulWidget> createState() {
  return CategoryListScreenState();
  }

}
class CategoryListScreenState extends State<CategoryListScreen>{
  TextEditingController search = new TextEditingController();
var list=[
 "All Categories",
  "Ambulance",
  "Blood Bank",
  "Care Taker",
  "Courier Service",
  "Doctor",
  "Electrician",
  "Emergency Medical Help",
  "Fire Brigade",
  "First Aid",
  "Foreign Exchange",
  "Fruit",
  "Grocery",
  "Helping Hands",
  "Hospital",
  "Housekeeping",
  "Lab & Radiology",
  "Labour / Helper",
  "Maid & Servant",
  "Medicine",
  "MIlk & Milk Products",
  "Nurse",
  "Oil & Ghee",
  "Pest Control",
  "Pets Care",
  "Plumber",
  "Police",
  "Repair & Laying",
  "Salons & Spa",
  "Tours & Travels",
  "Towing Service",
  "Vegetable",
  "Other"
];
  var _searchProduct=[];
@override
  void initState() {
    super.initState();
    if(!widget.showall)
    list.removeAt(0);
    onSearchTextChanged("");
  }
  @override
  Widget build(BuildContext context) {
 return Scaffold(backgroundColor: util.primaryYello,body: SafeArea( child: Column(children: <Widget>[
   SizedBox(height: 10,),

   Row(
     children: <Widget>[
       InkWell(
           onTap: () => Navigator.pop(context),
           child: SizedBox(height:40,width: 50,child: Icon(Icons.arrow_back))),
       Expanded(child: Container(height:40,padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
         child: TextFormField(
             controller: search,
    onChanged: onSearchTextChanged,
             style: TextStyle(backgroundColor: Colors.white),
             decoration: InputDecoration(
                 filled: true,
                 hintText: "Search",
                 fillColor: Colors.white,
                 contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                 border: OutlineInputBorder(
                   borderRadius: BorderRadius.circular(40),
                   borderSide: BorderSide.none,
                   gapPadding: 0,
                 ))),
       ),)
     ],
   ),
   SizedBox(height: 10,),
   Expanded(
     child: Container(
       padding: EdgeInsets.fromLTRB(10,0,10,10),
       child: ListView.separated(itemBuilder: (context, index) {
         return InkWell(onTap:(){
           Navigator.pop(context,{"selected":_searchProduct[index]});
         },child: Container(color:Colors.white,padding:EdgeInsets.all(10),child: Row(children: <Widget>[SizedBox(width: 18,height: 18,child: Image.asset("assets/images/ehh_logo_d.png")),VerticalDivider(width: 10,),Text(_searchProduct[index],style: TextStyle(fontSize: 20,color: util.primaryBrown),)],),));
       }, separatorBuilder: (context, index) {
         return  Container(color: util.primaryBrown,height: 1,);
       }, itemCount: _searchProduct.length),
     ),
   )
 ],),),);
  }
  onSearchTextChanged(String text) async {
  print("changed -$text-");
    _searchProduct.clear();
    if (text.isEmpty) {
      _searchProduct.addAll(list);
      setState(() {});
      return;
    }

    list.forEach((item) {
      if (item.toLowerCase().contains(text.toLowerCase())||item.toLowerCase()==text.toLowerCase()) {
        _searchProduct.add(item);
        print("matched $item  --- $text");
      }
    });

    setState(() {
    });
  }

}