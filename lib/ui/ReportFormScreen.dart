import 'dart:convert';

import 'package:ehelpinhands/data/FlutterSharedPrefrence.dart';
import 'package:ehelpinhands/data/NetworkApi.dart';
import 'package:ehelpinhands/networking/ApiProvider.dart';
import 'package:ehelpinhands/ui/CategoryListScreen.dart';
import 'package:ehelpinhands/ui/SearchLocationScreen.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:progress_dialog/progress_dialog.dart';

class ReportFormScreen extends StatefulWidget {
  String address;
  LocationData locationData;
  ReportFormScreen(this.address,this.locationData);
  @override
  _ReportFormScreenState createState() => _ReportFormScreenState();
}

class _ReportFormScreenState extends State<ReportFormScreen> {
  String _selectPriorityDropDown = null;
//String _selectCategoryDropdown = null;
  String _selectCateogry = null;

  LocationData _locationData;
  ProgressDialog pr;

  int _radioValue = 0;

  static DateTime _date = null;
  static TimeOfDay _time = null;
  String _selectlocation = null;

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
    });
  }

  bool agreeTerms = false;
  bool submitAnonmus = false;

  Future _locationButtonTapped() async {
    Map results = await Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) {
        return SearchLocationScreen();
      },
    ));

    if (results != null && results.containsKey('selection')) {
      setState(() {
        _selectlocation = results['selection'];
      });
    }
  }

  Future<Null> selecteDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.dark(),
          child: child,
        );
      },
    );
    picked != null
        ? setState(() {
      _date = picked;
    })
        : () {};
    print(_date.millisecondsSinceEpoch);
  }

  TextEditingController first_name = new TextEditingController();
  TextEditingController last_name = new TextEditingController();

  TextEditingController title = new TextEditingController();
  TextEditingController desc = new TextEditingController();
  TextEditingController contact_number = new TextEditingController();
  TextEditingController quantity = new TextEditingController();
  TextEditingController units = new TextEditingController();
  TextEditingController numberOfPeople = new TextEditingController();

  Future<Null> selecteTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.dark(),
          child: child,
        );
      },
    );
    picked != null
        ? setState(() {
      _time = picked;
    })
        : () {};
    print(_date.millisecondsSinceEpoch);
  }

  TextEditingController date_c;

  TextEditingController time_c;

  TextEditingController address;

  TextEditingController location = new TextEditingController();

  @override
  void initState() {
    super.initState();
//    _getLocation();
    _selectlocation=widget.address;
    _locationData=widget.locationData;


    FlutterSharedPrefrence().getFName().then((value){
      setState(() {
        first_name.text=value;
      });
    });
    FlutterSharedPrefrence().getLName().then((value){
      setState(() {
        last_name.text=value;
      });
    });
    FlutterSharedPrefrence().getPhone().then((value){
      setState(() {
        contact_number.text=value;
      });
    });
    address = new TextEditingController(text: _selectlocation);

  }

  @override
  Widget build(BuildContext mContext) {
    print("sdfsd");
    date_c = new TextEditingController(
      text: _date != null ? "${_date.day}-${_date.month}-${_date.year}" : "",
    );
    time_c = new TextEditingController(
      text: _time != null ? "${_time.hour}:${_time.minute}" : "",
    );
    var textBoxHeight = 40.0;
    var commonContentPadding = EdgeInsets.fromLTRB(10, 0, 10, 0);
    Future _selecteCategory() async {
      var results = await Navigator.of(context).push(MaterialPageRoute<dynamic>(
          builder: (context) => CategoryListScreen(false)));

      if (results != null && results.containsKey('selected')) {
        setState(() {
          print("selected " + results['selected']);
          _selectCateogry = results['selected'];
        });
      }
    }

    return WillPopScope(
      onWillPop: () => onBackPress(),
      child: Scaffold(
          backgroundColor: Colors.amberAccent,
          body: SafeArea(
              child: Builder(
                builder: (context) => SingleChildScrollView(
                    child: Stack(
                      children: <Widget>[
                        InkWell(
                            onTap: () async {
                              if (!checkIfAnyData()) {
                                onWillPop().then((value){
                                  print("Value $value");
                                  if(value)Navigator.of(mContext).pop();});

                              } else {
                                Navigator.pop(context);
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Icon(Icons.arrow_back),
                              ),
                            )),
                        Container(
                            padding: EdgeInsets.all(30),
                            child: Column(
                              children: <Widget>[
                                Center(
                                    child: SizedBox(
                                      height: 70,
                                      width: 70,
                                      child: Image.asset("assets/images/ehh_logo_d.png"),
                                    )),
                                SizedBox(
                                  height: 20,
                                ),
                                SizedBox(
                                  height: textBoxHeight,
                                  child: ButtonTheme(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(0.0),
                                    ),
                                    minWidth: double.infinity,
                                    child: Stack(
                                      children: <Widget>[
                                        RaisedButton(
                                          onPressed: () {
                                            _selecteCategory();
                                          },
                                          color: util.primaryBrown,
                                          child: Text(
                                            _selectCateogry == null
                                                ? "Select Category*"
                                                : _selectCateogry,
                                            style: TextStyle(
                                              fontSize: 18,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        Align(alignment:Alignment.centerRight,child: Row(mainAxisAlignment: MainAxisAlignment.end,
                                          children: <Widget>[

                                            Icon(Icons.arrow_drop_down,color: Colors.white,),SizedBox(width: 15,)
                                          ],
                                        ))
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: <Widget>[
                                    Container(
                                        height: textBoxHeight,
                                        width: textBoxHeight,
                                        child: Icon(
                                          Icons.arrow_forward_ios,
                                          color: Colors.white,
                                        ),
                                        color: util.primaryBrown),
                                    Expanded(
                                      child: SizedBox(
                                        height: textBoxHeight,
                                        child: Container(
                                          padding: EdgeInsets.fromLTRB(10, 0, 15, 0),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                          ),
                                          child: DropdownButton<String>(
                                            isExpanded: true,
                                            underline: Container(),
                                            value: _selectPriorityDropDown,
                                            hint: Text(
                                              "Select Report's Priority...*",
                                              style: TextStyle(
                                                  fontSize: 17, color: util.lightGrey),
                                            ),
                                            items: <String>[
                                              'Emergency',
                                              'Urgent - ASAP (As Soon As Possible)',
                                              'At the Earliest',
                                              "Whenever it's Possible"
                                            ].map((String value) {
                                              return new DropdownMenuItem<String>(
                                                value: value,
                                                child: new Text(value),
                                              );
                                            }).toList(),
                                            onChanged: (_) {
                                              setState(() {
                                                _selectPriorityDropDown = _;
                                              });
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: <Widget>[
                                    Container(
                                        height: textBoxHeight,
                                        width: textBoxHeight,
                                        child: Icon(
                                          Icons.arrow_forward_ios,
                                          color: Colors.white,
                                        ),
                                        color: util.primaryBrown),
                                    Expanded(
                                      child: SizedBox(
                                        height: textBoxHeight,
                                        child: TextFormField(
                                            textCapitalization:
                                            TextCapitalization.sentences,
                                            controller: title,
                                            style: TextStyle(
                                                backgroundColor: Colors.white),
                                            decoration: InputDecoration(
                                                filled: true,
                                                hintStyle: TextStyle(
                                                    fontSize: 17,
                                                    color: util.lightGrey),
                                                hintText: "Title of Report...*",
                                                fillColor: Colors.white,
                                                contentPadding: commonContentPadding,
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide.none,
                                                  gapPadding: 0,
                                                ))),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  color: Colors.white,
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                          height: textBoxHeight,
                                          width: textBoxHeight,
                                          child: Icon(
                                            Icons.arrow_forward_ios,
                                            color: Colors.white,
                                          ),
                                          color: util.primaryBrown),
                                      Expanded(
                                        child: SizedBox(
                                          child: TextFormField(
                                              textCapitalization:
                                              TextCapitalization.sentences,
                                              controller: desc,
                                              maxLines: 6,
                                              style: TextStyle(
                                                  backgroundColor: Colors.white),
                                              decoration: InputDecoration(
                                                  hintStyle: TextStyle(
                                                      fontSize: 17,
                                                      color: util.lightGrey),
                                                  filled: true,
                                                  hintText: "Descriptions...*",
                                                  fillColor: Colors.white,
                                                  contentPadding: EdgeInsets.fromLTRB(
                                                      10, 10, 20, 10),
                                                  border: OutlineInputBorder(
                                                    borderSide: BorderSide.none,
                                                    gapPadding: 0,
                                                    borderRadius:
                                                    BorderRadius.circular(0.0),
                                                  ))
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Checkbox(
                                      value: submitAnonmus,
                                      onChanged: (value) {
                                        setState(() {
                                          submitAnonmus = value;
                                        });
                                      },
                                    ),
                                    Text("Submit Report Anonymously...",style: TextStyle(fontSize: 18),)
                                  ],
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                !submitAnonmus?SizedBox(
                                  height: 10,
                                ):Container(),
                                !submitAnonmus? Row(
                                  children: <Widget>[
                                    Container(
                                      height: textBoxHeight,
                                      width: textBoxHeight,
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.white,
                                      ),
                                      color: util.primaryBrown,
                                    ),
                                    Expanded(
                                      child: SizedBox(
                                        height: textBoxHeight,
                                        child: TextFormField(
                                            textCapitalization:
                                            TextCapitalization.sentences,
                                            controller: first_name,
                                            style: TextStyle(
                                                color: util.primaryBrown,
                                                backgroundColor: Colors.white,
                                                fontSize: 17),
                                            decoration: InputDecoration(
                                                hintStyle: TextStyle(
                                                    fontSize: 17,
                                                    color: util.lightGrey),
                                                filled: true,
                                                hintText: "First Name",
                                                fillColor: Colors.white,
                                                contentPadding:
                                                EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide.none,
                                                ))),
                                      ),
                                    ),
                                    VerticalDivider(
                                      width: 10,
                                    ),
                                    Container(
                                      height: textBoxHeight,
                                      width: textBoxHeight,
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.white,
                                      ),
                                      color: util.primaryBrown,
                                    ),
                                    Expanded(
                                      child: SizedBox(
                                        height: textBoxHeight,
                                        child: TextFormField(
                                            textCapitalization:
                                            TextCapitalization.sentences,
                                            controller: last_name,
                                            style: TextStyle(
                                                backgroundColor: Colors.white),
                                            decoration: InputDecoration(
                                                filled: true,
                                                hintText: "Last Name",
                                                fillColor: Colors.white,
                                                hintStyle: TextStyle(
                                                    fontSize: 17,
                                                    color: util.lightGrey),
                                                contentPadding:
                                                EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide.none,
                                                  gapPadding: 0,
                                                ))),
                                      ),
                                    ),
                                  ],
                                ):Container(),
                                !submitAnonmus?SizedBox(
                                  height: 20,
                                ):Container(),
                                !submitAnonmus?Row(crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      height: textBoxHeight,
                                      width: textBoxHeight,
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.white,
                                      ),
                                      color: util.primaryBrown,
                                    ),
                                    Expanded(
                                      child: SizedBox(
                                        height: textBoxHeight+18,
                                        child: TextFormField(
                                            maxLength: 10,
                                            controller: contact_number,
                                            keyboardType: TextInputType.number,
                                            style: TextStyle(
                                                backgroundColor: Colors.white),
                                            decoration: InputDecoration(
                                                filled: true,
                                                hintStyle: TextStyle(
                                                    fontSize: 17,
                                                    color: util.lightGrey),
                                                hintText: "Contact Number",
                                                fillColor: Colors.white,
                                                contentPadding: commonContentPadding,
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide.none,
                                                  gapPadding: 0,
                                                  borderRadius:
                                                  BorderRadius.circular(3.0),
                                                ))),
                                      ),
                                    ),
                                  ],
                                ):Container(),
                                !submitAnonmus?SizedBox(
                                  height: 20,
                                ):Container(),
                                !submitAnonmus? Row(
                                  children: <Widget>[
                                    Container(
                                        height: textBoxHeight,
                                        width: textBoxHeight,
                                        child: Icon(
                                          Icons.arrow_forward_ios,
                                          color: Colors.white,
                                        ),
                                        color: util.primaryBrown),
                                    Expanded(
                                      child: SizedBox(
                                        height: textBoxHeight,
                                        child: TextFormField(
                                            textCapitalization:
                                            TextCapitalization.sentences,
                                            controller: location,
                                            style: TextStyle(
                                                backgroundColor: Colors.white),
                                            decoration: InputDecoration(
                                                filled: true,
                                                hintStyle: TextStyle(
                                                    fontSize: 17,
                                                    color: util.lightGrey),
                                                hintText:
                                                "Door No., Floor No., Building No./Name, City, Pincode, Landmark",
                                                fillColor: Colors.white,
                                                contentPadding: commonContentPadding,
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide.none,
                                                  gapPadding: 0,
                                                  borderRadius:
                                                  BorderRadius.circular(0.0),
                                                ))),
                                      ),
                                    ),
                                  ],
                                ):Container(),
                                !submitAnonmus?TextFormField(
                                    controller: address,
                                    maxLines: 2,
                                    onTap: () {
                                      _locationButtonTapped();
                                    },
                                    readOnly: true,
                                    keyboardType: TextInputType.number,
                                    style: TextStyle(backgroundColor: Colors.white),
                                    decoration: InputDecoration(
                                        filled: true,
                                        hintStyle: TextStyle(
                                            fontSize: 17, color: util.lightGrey),
                                        hintText: "Request Location* / Address Line 2",
                                        fillColor: Colors.white,
                                        contentPadding:
                                        EdgeInsets.fromLTRB(50, 0, 10, 0),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                          gapPadding: 0,
                                          borderRadius: BorderRadius.circular(0),
                                        ))):Container(),

                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Checkbox(
                                      value: agreeTerms,
                                      onChanged: (value) {
                                        setState(() {
                                          agreeTerms = value;
                                        });
                                      },
                                    ),
                                    Text("I Agree, Accept & Confirm all T&C*")
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                ButtonTheme(
                                  padding: EdgeInsets.all(15),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(125.0),
                                  ),
                                  minWidth: double.infinity,
                                  child: RaisedButton(
                                    onPressed: () async {
                                      if (validate(context)) {
                                        pr = new ProgressDialog(context);
                                        pr.show();
                                        if(!buttonClicked) {
                                          NetworkApi.createReportRequest(
                                              _locationData.latitude.toString(),
                                              _locationData.longitude.toString(),
                                              _selectCateogry,
                                              first_name.text,
                                              last_name.text,
                                              _selectPriorityDropDown,
                                              title.text,
                                              desc.text,
                                              !submitAnonmus ? contact_number.text : " ",
                                              !submitAnonmus ? location.text + "," + address.text : "-",
                                              submitAnonmus)
                                              .then((value) async {
                                            buttonClicked = false;
                                            pr.hide().then((isHidden) {
                                              print(isHidden);
                                              var response =
                                              ApiProvider.my_response(value);
                                              if (response["status"] == true) {
                                                Scaffold.of(context)
                                                    .showSnackBar(SnackBar(
                                                  content: Text(
                                                      "Your request has been submited..."),
                                                ));
                                                Navigator.pop(mContext, {"value": true});
                                              } else {
                                                showSnackBar(
                                                    response["message"], context);
                                              }
                                            });
                                            print(jsonDecode(value.body));
                                          });
                                        }
                                      }
                                    },
                                    color: util.primaryBrown,
                                    child: Text(
                                      "Submit",
                                      style: TextStyle(
                                        fontSize: 24,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ))
                      ],
                    )),
              ))),
    );
    throw UnimplementedError();
  }
  var buttonClicked=false;
  Future<LocationData> _getLocation() async {
    Location location = new Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return null;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return null;
      }
    }
    _locationData = await location.getLocation();
    print("location data ${_locationData.latitude},${_locationData.longitude}");
    return _locationData;
  }

  @override
  void dispose() {
    _selectPriorityDropDown = null;
    _date = null;
    _time = null;
    super.dispose();
  }

  bool validate(BuildContext c) {
    if (_selectCateogry == null) {
      showSnackBar("Select Category", c);
      return false;
    }else if (_selectPriorityDropDown == null) {
      showSnackBar("Selected Priority", c);

      return false;
    }  else if (title.text.length == 0) {
      showSnackBar("Title of Need can't be empty", c);

      return false;
    }else if (desc.text.length == 0) {
      showSnackBar("Description can't be empty", c);

      return false;
    }  else if (first_name.text.length == 0&&!submitAnonmus) {
      showSnackBar("First Name can't be empty", c);

      return false;
    } else if (last_name.text.length == 0&&!submitAnonmus) {
      showSnackBar("Last Name can't be empty", c);

      return false;
    }  else if (contact_number.text.length < 10&&!submitAnonmus) {
      showSnackBar("Contact No. is invalid", c);

      return false;
    } else if (location.text.length == 0&&!submitAnonmus) {
      showSnackBar("Location can't be empty", c);

      return false;
    } else if (address.text.length == 0&&!submitAnonmus) {
      showSnackBar("Address can't be empty", c);
      return false;
    } else if (!agreeTerms) {
      showSnackBar("Please Agree Teams and Conditions", c);

      return false;
    } else {
      return true;
    }
  }

  showSnackBar(String text, BuildContext context) {
    Scaffold.of(context).showSnackBar(SnackBar(
      backgroundColor: util.urgentRed,duration: Duration(seconds: 1),
      content: Text(text),
    ));
  }

  onBackPress() {
    if (!checkIfAnyData()) {
      return onWillPop();
//      Scaffold.of(context).showSnackBar(SnackBar(
//        backgroundColor: util.urgentRed,
//        content: Text("Are you sure you want to go back?"),action: SnackBarAction(onPressed: (){
//          Navigator.pop(context);
//      }, label: "Yes",),
//      ),);
    } else {
      Navigator.pop(context);
    }
  }

  bool checkIfAnyData() {
    if (_selectCateogry != null) {
      return false;
    } else if (first_name.text.length != 0) {
      return false;
    } else if (last_name.text.length != 0) {
      return false;
    } else if (_selectPriorityDropDown != null) {
      return false;
    } else if (_date != null) {
      return false;
    } else if (_time != null) {
      return false;
    } else if (title.text.length != 0) {
      return false;
    } else if (desc.text.length != 0) {
      return false;
    } else if (contact_number.text.length > 0) {
      return false;
    } else if (location.text.length != 0) {
      return false;
    } else if (address.text.length != 0) {
      return false;
    } else if (quantity.text.length != 0) {
      return false;
    } else if (units.text.length != 0) {
      return false;
    } else if (numberOfPeople.text.length != 0) {
      return false;
    } else if (_radioValue != 0) {
      return false;
    } else if (agreeTerms) {
      return false;
    } else if (_selectlocation != null) {
      return false;
    } else {
      return true;
    }
  }

  DateTime currentBackPressTime;

  Future<bool> onWillPop() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Confirmation!!!'),
            content: Text(
                'Are you sure you want to go back, All of your filled data will be lost.'),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'NO',
                  style: TextStyle(color: util.primaryBrown),
                ),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              FlatButton(
                child: Text('YES', style: TextStyle(color: util.primaryBrown)),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
            ],
          );
        });
  }

}
