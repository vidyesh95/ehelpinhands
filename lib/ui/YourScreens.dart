import 'package:ehelpinhands/ui/home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class YourScreens extends StatefulWidget {
  ServiceType type;
  YourScreens(this.type);
  @override
  _YourScreensState createState() => _YourScreensState();
}

class _YourScreensState extends State<YourScreens> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title: Text("Your " + getTitle(widget.type)),), body: SafeArea(child: Container()),);
  }

  getTitle(ServiceType type) {
    switch (type) {
      case ServiceType.NEEDS:
        return "Needs";
      case ServiceType.SUGGESION:
        return "Suggesions";
      case ServiceType.REPORT:
        return "Reports";
    }
  }
}

