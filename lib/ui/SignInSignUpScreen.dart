import 'package:ehelpinhands/data/FlutterSharedPrefrence.dart';
import 'package:ehelpinhands/repository/UserRepository.dart';
import 'package:ehelpinhands/ui/ProfileScreen.dart';
import 'package:ehelpinhands/ui/home_page.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:progress_dialog/progress_dialog.dart';

class SignInSignUpScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SignInSignUpScreenState();
  }
}

class SignInSignUpScreenState extends State<SignInSignUpScreen> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  var commonContentPadding = EdgeInsets.fromLTRB(10, 0, 10, 0);

  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController mobile = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController cpassword = TextEditingController();
  TextEditingController dob = TextEditingController();
  TextEditingController date_c;

  bool agreeTerms = false;
  String _genderSelector = null;
  static DateTime _date = null;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ProgressDialog pr = new ProgressDialog(context);

    date_c = new TextEditingController(
      text: _date != null ? "${_date.day}-${_date.month}-${_date.year}" : "",
    );
    return Scaffold(
      backgroundColor: util.primaryYello,
      body: Builder(
        builder: (context) => SafeArea(
            child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              padding: EdgeInsets.fromLTRB(32, 56, 32, 56),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 70,
                    width: 70,
                    child: Image.asset("assets/images/ehh_logo_d.png"),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                            textCapitalization: TextCapitalization.sentences,
                            controller: firstName,
                            style: TextStyle(
                                color: util.primaryBrown, fontSize: 18),
                            decoration: InputDecoration(
                                filled: true,
                                hintStyle: TextStyle(
                                    fontSize: 18, color: util.lightGrey),
                                hintText: "First Name",
                                fillColor: Colors.white,
                                prefixIcon: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Icon(
                                      Icons.person_outline,
                                      color: util.lightGrey,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Container(
                                      width: 1,
                                      height: 25,
                                      color: util.lightGrey,
                                    ),
                                    SizedBox(
                                      width: 12,
                                    )
                                  ],
                                ),
                                contentPadding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(40),
                                      bottomLeft: Radius.circular(40)),
                                  borderSide: BorderSide.none,
                                ))),
                      ),
                      VerticalDivider(
                        width: 5,
                      ),
                      Expanded(
                        child: TextFormField(
                            textCapitalization: TextCapitalization.sentences,
                            controller: lastName,
                            style: TextStyle(
                                color: util.primaryBrown, fontSize: 16),
                            decoration: InputDecoration(
                                filled: true,
                                hintText: "Last Name",
                                fillColor: Colors.white,
                                hintStyle: TextStyle(
                                    fontSize: 18, color: util.lightGrey),
                                contentPadding:
                                    EdgeInsets.fromLTRB(24, 0, 10, 0),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(40),
                                      bottomRight: Radius.circular(40)),
                                ))),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  SizedBox(
                    child: TextFormField(
                        textCapitalization: TextCapitalization.sentences,
                        controller: mobile,
                        maxLength: 10,
                        keyboardType: TextInputType.number,
                        style: TextStyle(fontSize: 18),
                        decoration: InputDecoration(
                            filled: true,
                            hintStyle:
                                TextStyle(fontSize: 18, color: util.lightGrey),
                            hintText: "Mobile No.",
                            fillColor: Colors.white,
                            prefixIcon: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                SizedBox(
                                  width: 15,
                                ),
                                Icon(
                                  Icons.phone,
                                  color: util.lightGrey,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Container(
                                  width: 1,
                                  height: 25,
                                  color: util.lightGrey,
                                ),
                                SizedBox(
                                  width: 12,
                                )
                              ],
                            ),
                            contentPadding: commonContentPadding,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(40),
                              borderSide: BorderSide.none,
                            ))),
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  SizedBox(
                    child: TextFormField(
                        controller: email,
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(fontSize: 20),
                        decoration: InputDecoration(
                            filled: true,
                            hintStyle:
                                TextStyle(fontSize: 20, color: util.lightGrey),
                            hintText: "Email ID",
                            fillColor: Colors.white,
                            prefixIcon: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                SizedBox(
                                  width: 15,
                                ),
                                Icon(
                                  Icons.email,
                                  color: util.lightGrey,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Container(
                                  width: 1,
                                  height: 25,
                                  color: util.lightGrey,
                                ),
                                SizedBox(
                                  width: 12,
                                )
                              ],
                            ),
                            contentPadding: commonContentPadding,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(40),
                              borderSide: BorderSide.none,
                            ))),
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  SizedBox(
                    child: TextFormField(
                        textCapitalization: TextCapitalization.sentences,
                        controller: password,
                        obscureText: true,
                        keyboardType: TextInputType.text,
                        style: TextStyle(fontSize: 20),
                        decoration: InputDecoration(
                            filled: true,
                            hintStyle: TextStyle(
                              fontSize: 20,
                              color: util.lightGrey,
                            ),
                            hintText: "Password",
                            fillColor: Colors.white,
                            prefixIcon: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                SizedBox(
                                  width: 15,
                                ),
                                Icon(
                                  Icons.lock_outline,
                                  color: util.lightGrey,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Container(
                                  width: 1,
                                  height: 25,
                                  color: util.lightGrey,
                                ),
                                SizedBox(
                                  width: 12,
                                )
                              ],
                            ),
                            contentPadding: commonContentPadding,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(40),
                              borderSide: BorderSide.none,
                            ))),
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  SizedBox(
                    child: TextFormField(
                        textCapitalization: TextCapitalization.sentences,
                        controller: cpassword,
                        obscureText: true,
                        keyboardType: TextInputType.text,
                        style: TextStyle(fontSize: 20),
                        decoration: InputDecoration(
                            filled: true,
                            hintStyle: TextStyle(
                              fontSize: 20,
                              color: util.lightGrey,
                            ),
                            hintText: "Confirm Password",
                            fillColor: Colors.white,
                            prefixIcon: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                SizedBox(
                                  width: 15,
                                ),
                                Icon(
                                  Icons.lock_outline,
                                  color: util.lightGrey,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Container(
                                  width: 1,
                                  height: 25,
                                  color: util.lightGrey,
                                ),
                                SizedBox(
                                  width: 12,
                                )
                              ],
                            ),
                            contentPadding: commonContentPadding,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(40),
                              borderSide: BorderSide.none,
                            ))),
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(40),
                                topLeft: Radius.circular(40)),
                            color: Colors.white,
                          ),
                          child: Row(
                            children: <Widget>[
                              SizedBox(
                                width: 15,
                              ),
                              Icon(
                                Icons.person_outline,
                                color: util.lightGrey,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                width: 1,
                                height: 25,
                                color: util.lightGrey,
                              ),
                              SizedBox(
                                width: 12,
                              ),
                              Expanded(
                                child: DropdownButton<String>(
                                  isExpanded: true,
                                  underline: Container(),
                                  value: _genderSelector,
                                  hint: Text(
                                    "Gender",
                                    style: TextStyle(
                                        fontSize: 16, color: util.lightGrey),
                                  ),
                                  items: <String>['Male', 'Female', 'Others']
                                      .map((String value) {
                                    return new DropdownMenuItem<String>(
                                      value: value,
                                      child: new Text(value),
                                    );
                                  }).toList(),
                                  onChanged: (_) {
                                    setState(() {
                                      _genderSelector = _;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        child: TextFormField(
                            textCapitalization: TextCapitalization.sentences,
                            controller: date_c,
                            readOnly: true,
                            onTap: () {
                              selecteDate(context);
                            },
                            style: TextStyle(backgroundColor: Colors.white),
                            decoration: InputDecoration(
                                filled: true,
                                hintText: "D.O.B.",
                                fillColor: Colors.white,
                                hintStyle: TextStyle(
                                    fontSize: 17, color: util.lightGrey),
                                contentPadding:
                                    EdgeInsets.fromLTRB(20, 0, 10, 0),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(40),
                                      bottomRight: Radius.circular(40)),
                                ))),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Checkbox(
                        value: agreeTerms,
                        onChanged: (value) {
                          setState(() {
                            agreeTerms = value;
                          });
                        },
                      ),
                      Text("I Agree, Accept & Confirm all T&C*")
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  InkWell(
                    onTap: () async {
                      if (validate(context)) {
                        pr.show();
                        var data = {
                          "first_name": firstName.text,
                          "last_name": lastName.text,
                          "email": email.text,
                          "mobile": mobile.text,
                          "password": password.text,
                          "gender": _genderSelector,
                          "dob": dob.text,
                          "device_name": "",
                          "fcm_id": await FlutterSharedPrefrence().getFcmId()
                        };
                        UserRepository().signUp(data).then((value) async {
                          pr.hide().then((isHidden) {
                            print(isHidden);
                            if (value.status) {
                              var id = value.data.id;
                              var first_name = value.data.firstName;
                              var last_name = value.data.lastName;
                              var email = value.data.email;
                              var phone_number = value.data.phoneNumber;
                              var is_blocked = value.data.isBlocked;
                              FlutterSharedPrefrence().setUserData(
                                  id.toString(),
                                  first_name,
                                  last_name,
                                  email,
                                  phone_number,
                                  "",
                                  "",
                                  "",
                                  value.data.gender,
                                  value.data.dob,
                                  is_blocked);
                              if (is_blocked) {
                                Fluttertoast.showToast(
                                    msg: "You have been block by the admin!!!");
                                return;
                              }

                              Navigator.of(context).pushAndRemoveUntil(
                                  MaterialPageRoute(
                                builder: (context) {
                                  return ProfileScreen();
                                },
                              ), (Route<dynamic> route) => false);
                              Navigator.of(context).pushAndRemoveUntil(
                                  MaterialPageRoute(
                                builder: (context) {
                                  return HomePage();
                                },
                              ), (Route<dynamic> route) => false);
                            } else {
                              showSnackBar(value.message, context);
                            }
                          });
                        });
                      }
                    },
                    child: Container(
                      child: Stack(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              ButtonTheme(
                                padding: EdgeInsets.fromLTRB(0, 7, 30, 7),
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topLeft: Radius.circular(125),
                                      bottomLeft: Radius.circular(125)),
                                ),
                                child: Expanded(
                                  child: RaisedButton(
                                    disabledColor: Color(0xFFFFF6CF),
                                    color: Color(0xFFFFF6CF),
                                    child: Text(
                                      "Sign Up",
                                      style: TextStyle(
                                        fontSize: 24,
                                        color: util.primaryBrown,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              ButtonTheme(
                                padding: EdgeInsets.fromLTRB(40, 7, 20, 7),
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topRight: Radius.circular(125),
                                      bottomRight: Radius.circular(125)),
                                ),
                                child: Expanded(
                                  child: RaisedButton(
                                    disabledColor: Color(0xFFFFF6CF),
                                    color: Color(0xFFFFF6CF),
                                    child: Text(
                                      " Now  ",
                                      style: TextStyle(
                                        fontSize: 28,
                                        color: util.primaryBrown,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            height: 85,
                            width: 85,
                            child: Center(
                              child: Icon(
                                Icons.person_add,
                                size: 60,
                              ),
                            ),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xFFFFF6CF)),
                          ),
                        ],
                        alignment: Alignment.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: InkWell(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Icon(
                    Icons.arrow_back_ios,
                    size: 30,
                  ),
                ),
                onTap: () => Navigator.pop(context),
              ),
            ),
          ],
        )
//      child: Center(child: Container( child:_signInButton(),))
            ),
      ),
    );
  }

//  return await NetworkApi.verifyUser(token,androidInfo.model,"-");
  void signOutGoogle() async {
    await googleSignIn.signOut();

    print("User Sign Out");
  }

  bool validate(BuildContext c) {
    if (firstName.text.length == 0) {
      showSnackBar("First Name can't be empty", c);

      return false;
    } else if (lastName.text.length == 0) {
      showSnackBar("Last Name can't be empty", c);

      return false;
    } else if (mobile.text.length != 10) {
      showSnackBar("Invalid Mobile No.", c);

      return false;
    } else if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email.text)) {
      showSnackBar("Invalid Email Id.", c);

      return false;
    } else if (password.text.length == 0) {
      showSnackBar("Password can't be empty", c);

      return false;
    } else if (password.text.length < 8) {
      showSnackBar("Password should countain atleast 8 characters", c);

      return false;
    } else if (cpassword.text.length == 0) {
      showSnackBar("Confirm Password can't be empty", c);

      return false;
    } else if (cpassword.text != password.text) {
      showSnackBar("Confirm Password does not match", c);
      return false;
    } else if (_genderSelector == null) {
      showSnackBar("Selected Gender", c);

      return false;
    } else if (_date == null) {
      showSnackBar("Select DoB", c);

      return false;
    } else if (!agreeTerms) {
      showSnackBar("Please agree Teams and conditions", c);
      return false;
    } else {
      return true;
    }
  }

  Future<Null> selecteDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1800),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.dark(),
          child: child,
        );
      },
    );
    picked != null
        ? setState(() {
            _date = picked;
          })
        : () {};
    print(_date.millisecondsSinceEpoch);
  }

  showSnackBar(String text, BuildContext context) {
    Scaffold.of(context).showSnackBar(SnackBar(
      backgroundColor: util.urgentRed,
      content: Text(text),
    ));
  }
}
