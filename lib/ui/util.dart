 import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';

class util{
   static  Color urgentRed =  Color(0xFFff0000);
   static  Color earliestGreen =  Color(0xFF007C33);
   static  Color wheneverBlue =  Color(0xFF000080);

   static  Color primaryYello =  Color(0xFFfdd023);
   static  Color primaryBrown =  Color(0xFF322701);

   static Color darkGrey =  Color(0xFF727272);
   static Color lightGrey =  Color(0xFFc2c2c2);

  static  Future<Address> getUserLocation( LocationData myLocation) async {//call this async method from whereever you need

//      LocationData myLocation;
//      String error;
//      Location location = new Location();
//      try {
//         myLocation = await location.getLocation();
//      } on PlatformException catch (e) {
//         if (e.code == 'PERMISSION_DENIED') {
//            error = 'please grant permission';
//            print(error);
//         }
//         if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
//            error = 'permission denied- please enable it from app settings';
//            print(error);
//         }
//         myLocation = null;
//      }
//      currentLocation = myLocation;
      final coordinates = new Coordinates(
          myLocation.latitude, myLocation.longitude);
      var addresses = await Geocoder.local.findAddressesFromCoordinates(
          coordinates);
      var first = addresses.first;
      print(' ${first.locality}-, ${first.adminArea}-,${first.subLocality}-, ${first.subAdminArea}-,${first.addressLine}-, ${first.featureName}-,${first.thoroughfare}-, ${first.subThoroughfare}');
      return first;
   }

     static launchURL(String url) async {
     if (await canLaunch(url)) {
       await launch(url);
     } else {
       throw 'Could not launch $url';
     }
   }
}