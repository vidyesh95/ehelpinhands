import 'package:ehelpinhands/ui/GroupTile.dart';
import 'package:ehelpinhands/ui/ProfileScreen.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

class GroupScreen extends StatefulWidget  {
  @override
  _GroupScreenState createState() => _GroupScreenState();
}

class _GroupScreenState extends State<GroupScreen> with TickerProviderStateMixin{
  TabController defaultTabController;
  ProgressDialog pr;

  var all=[
    GroupModel("Group","abc "),
    GroupModel("Group","abc "),
    GroupModel("Group","abc "),
    GroupModel("Group","abc "),
    GroupModel("Group","abc "),
  ];
  @override
  void initState() {
    super.initState();
    pr = new ProgressDialog(context);
    defaultTabController = TabController(vsync: this, length: 2);
    defaultTabController.addListener(() {
      setState(() {

      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      backgroundColor: util.primaryBrown,
      body: Builder(
        builder: (context) => SafeArea(
            child: Column(
              children: <Widget>[
                Container(
                  color: Colors.white,
                  child: TabBar(
                    controller: defaultTabController,
                    indicatorWeight: 3,
                    labelColor: Colors.black,
                    labelStyle: TextStyle(fontWeight: FontWeight.bold),
                    unselectedLabelColor: Colors.black45,
                    indicatorColor: Colors.yellow,
                    isScrollable: true,
                    tabs: [
                      Container(
                        height: 35,
                        child: new Tab(
                          child: Text(
                            "Manage Groups",
                          ),
                        ),
                      ),
                      Container(
                        height: 35,
                        child: new Tab(
                          child: Text(
                            "Request Group",
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  color: Colors.brown,
                  height: 2,
                ),

                TabBarView(
                  controller: defaultTabController,
                  children: <Widget>[
                    ListView.separated(separatorBuilder:(context, index) =>  SizedBox(height: 10,),itemBuilder: (context, index) => GroupTile(all[index],index,(value){
                    }),itemCount: all.length,),
                    ListView.separated(separatorBuilder:(context, index) =>  SizedBox(height: 10,),itemBuilder: (context, index) => GroupTile(all[index],index,(value){
                    }),itemCount: all.length,),
                  ],
                ),
              ],
            )
//      child: Center(child: Container( child:_signInButton(),))
        ),
      ),
    );
  }
}
