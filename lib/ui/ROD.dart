import 'dart:convert';
import 'dart:ui';

import 'package:ehelpinhands/data/FlutterSharedPrefrence.dart';
import 'package:ehelpinhands/data/NetworkApi.dart';
import 'package:ehelpinhands/networking/ApiProvider.dart';
import 'package:ehelpinhands/ui/CategoryListScreen.dart';
import 'package:ehelpinhands/ui/GroupListScreen.dart';
import 'package:ehelpinhands/ui/SearchLocationScreen.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:progress_dialog/progress_dialog.dart';

class RequestOnDemand extends StatefulWidget {
  String _priorityType = "1";

  String address;
  LocationData locationData;

  RequestOnDemand(this.address, this.locationData);

  @override
  State<StatefulWidget> createState() {
    return RequestOnDemandState();
    throw UnimplementedError();
  }
}

String _selectPriorityDropDown = null;
//String _selectCategoryDropdown = null;
String _selectCateogry = null;
String _selectGroup = null;

class RequestOnDemandState extends State<RequestOnDemand> {
  LocationData _locationData;
  ProgressDialog pr;

  int _radioValue = 0;

  static DateTime _date = null;
  static TimeOfDay _time = null;
  String _selectlocation = null;

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
    });
  }

  bool agreeTerms = false;

  Future _locationButtonTapped() async {
    Map results = await Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) {
        return SearchLocationScreen();
      },
    ));

    if (results != null && results.containsKey('selection')) {
      setState(() {
        _selectlocation = results['selection'];
      });
    }
  }

  Future<Null> selecteDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.dark(),
          child: child,
        );
      },
    );
    picked != null
        ? setState(() {
            _date = picked;
          })
        : () {};
    print(_date.millisecondsSinceEpoch);
  }

  TextEditingController first_name = new TextEditingController();
  TextEditingController last_name = new TextEditingController();

  TextEditingController title = new TextEditingController();
  TextEditingController desc = new TextEditingController();
  TextEditingController contact_number = new TextEditingController();
  TextEditingController quantity = new TextEditingController();
  TextEditingController units = new TextEditingController();
  TextEditingController numberOfPeople = new TextEditingController();

  Future<Null> selecteTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.dark(),
          child: child,
        );
      },
    );
    picked != null
        ? setState(() {
            _time = picked;
          })
        : () {};
    print(_date.millisecondsSinceEpoch);
  }

  TextEditingController date_c;

  TextEditingController time_c;

  TextEditingController address;

  TextEditingController location = new TextEditingController();
  bool submitAnonmus = false;
  var buttonClicked = false;

  @override
  void initState() {
    super.initState();
//    _getLocation();
    _selectlocation = widget.address;
    _locationData = widget.locationData;
    _selectCateogry = null;
    _selectGroup = null;

    FlutterSharedPrefrence().getFName().then((value) {
      setState(() {
        first_name.text = value;
      });
    });
    FlutterSharedPrefrence().getLName().then((value) {
      setState(() {
        last_name.text = value;
      });
    });
    FlutterSharedPrefrence().getPhone().then((value) {
      setState(() {
        contact_number.text = value;
      });
    });
    address = new TextEditingController(text: _selectlocation);
  }

  BuildContext mContext = null;
  _selecteCategory() async {
    var results = await Navigator.of(context).push(MaterialPageRoute<dynamic>(
        builder: (context) => CategoryListScreen(false)));

      if (results != null && results.containsKey('selected')) {
      setState(() {
        print("selected " + results['selected']);
        _selectCateogry = results['selected'];
      });
    }
  }
  _selecteGroup() async {
    var results = await Navigator.of(context).push(MaterialPageRoute<dynamic>(
        builder: (context) => GroupListScreen(false)));

    if (results != null && results.containsKey('selected')) {
      setState(() {
        print("selected " + results['selected']);
        _selectGroup = results['selected'];
      });
    }
  }


    @override
  Widget build(BuildContext mContext) {
    this.mContext = mContext;
    print("sdfsd");
    date_c = new TextEditingController(
      text: _date != null ? "${_date.day}-${_date.month}-${_date.year}" : "",
    );
    time_c = new TextEditingController(
      text: _time != null ? "${_time.hour}:${_time.minute}" : "",
    );
    var textBoxHeight = 40.0;
    var commonContentPadding = EdgeInsets.fromLTRB(10, 0, 10, 0);

    return WillPopScope(
      onWillPop: () => onBackPress(),
      child: Scaffold(
          backgroundColor: Colors.amberAccent,
          body: SafeArea(
              child: Builder(
            builder: (context) => SingleChildScrollView(
                child: Stack(
              children: <Widget>[
                InkWell(
                    onTap: () async {
                      if (!checkIfAnyData()) {
                        onWillPop().then((value) {
                          print("Value $value");
                          if (value) Navigator.of(mContext).pop();
                        });
                      } else {
                        Navigator.pop(this.mContext);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(24, 24, 24, 24),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Icon(Icons.arrow_back),
                      ),
                    )),
                Container(
                    padding: EdgeInsets.fromLTRB(24, 56, 24, 56),
                    child: Column(
                      children: <Widget>[
                        Center(
                            child: SizedBox(
                          height: 70,
                          width: 70,
                          child: Image.asset("assets/images/ehh_logo_d.png"),
                        )),
                        SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: textBoxHeight,
                          child: ButtonTheme(
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(0.0),
                            ),
                            minWidth: double.infinity,
                            child: Stack(
                              children: <Widget>[
                                RaisedButton(
                                  onPressed: () {
                                    _selecteCategory();
                                  },
                                  color: util.primaryBrown,
                                  child: Text(
                                    _selectCateogry == null
                                        ? "Select Category"
                                        : _selectCateogry,
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Icon(
                                          Icons.arrow_drop_down,
                                          color: Colors.white,
                                        ),
                                        SizedBox(
                                          width: 15,
                                        )
                                      ],
                                    ))
                              ],
                            ),
                          ),
                        ),

                        SizedBox(
                          height: 20,
                        ),

                        //// ++++++++++++++Select Group Drop Down ++++++++++++++

                        SizedBox(
                          height: textBoxHeight,
                          child: ButtonTheme(
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(0.0),
                            ),
                            minWidth: double.infinity,
                            child: Stack(
                              children: <Widget>[
                                RaisedButton(
                                  onPressed: () {
                                    _selecteGroup();
                                  },
                                  color: util.primaryBrown,
                                  child: Text(
                                    _selectCateogry == null
                                        ? "Select Groups"
                                        : _selectGroup,
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Icon(
                                          Icons.arrow_drop_down,
                                          color: Colors.white,
                                        ),
                                        SizedBox(
                                          width: 15,
                                        )
                                      ],
                                    ))
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                                height: textBoxHeight,
                                width: textBoxHeight,
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.white,
                                ),
                                color: util.primaryBrown),
                            Expanded(
                              child: SizedBox(
                                height: textBoxHeight,
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(10, 0, 15, 0),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                  ),
                                  child: DropdownButton<String>(
                                    isExpanded: true,
                                    underline: Container(),
                                    value: _selectPriorityDropDown,
                                    hint: Text(
                                      "Select Need's Priority...*",
                                      style: TextStyle(
                                          fontSize: 17, color: util.lightGrey),
                                    ),
                                    items: <String>[
                                      'Emergency',
                                      'Urgent - ASAP (As Soon As Possible)',
                                      'At the Earliest',
                                      "Whenever it's Possible"
                                    ].map((String value) {
                                      return new DropdownMenuItem<String>(
                                        value: value,
                                        child: new Text(value),
                                      );
                                    }).toList(),
                                    onChanged: (_) {
                                      setState(() {
                                        _selectPriorityDropDown = _;
                                      });
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                                height: textBoxHeight,
                                width: textBoxHeight,
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.white,
                                ),
                                color: util.primaryBrown),
                            Expanded(
                              child: SizedBox(
                                height: textBoxHeight,
                                child: TextFormField(
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    controller: title,
                                    style: TextStyle(
                                        backgroundColor: Colors.white),
                                    decoration: InputDecoration(
                                        filled: true,
                                        hintStyle: TextStyle(
                                            fontSize: 17,
                                            color: util.lightGrey),
                                        hintText: "Title of Need...*",
                                        fillColor: Colors.white,
                                        contentPadding: commonContentPadding,
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                          gapPadding: 0,
                                        ))),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          color: Colors.white,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                  height: textBoxHeight,
                                  width: textBoxHeight,
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.white,
                                  ),
                                  color: util.primaryBrown),
                              Expanded(
                                child: SizedBox(
                                  child: TextFormField(
                                      textCapitalization:
                                          TextCapitalization.sentences,
                                      controller: desc,
                                      maxLines: 6,
                                      style: TextStyle(
                                          backgroundColor: Colors.white),
                                      decoration: InputDecoration(
                                          hintStyle: TextStyle(
                                              fontSize: 17,
                                              color: util.lightGrey),
                                          filled: true,
                                          hintText: "Descriptions...*",
                                          fillColor: Colors.white,
                                          contentPadding: EdgeInsets.fromLTRB(
                                              10, 10, 20, 10),
                                          border: OutlineInputBorder(
                                            borderSide: BorderSide.none,
                                            gapPadding: 0,
                                            borderRadius:
                                                BorderRadius.circular(0.0),
                                          ))),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                                height: textBoxHeight,
                                width: textBoxHeight,
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.white,
                                ),
                                color: util.primaryBrown),
                            Expanded(
                              child: SizedBox(
                                height: textBoxHeight,
                                child: TextFormField(
                                    controller: date_c,
                                    readOnly: true,
                                    onTap: () {
                                      selecteDate(context);
                                    },
                                    style: TextStyle(
                                        backgroundColor: Colors.white),
                                    decoration: InputDecoration(
                                        filled: true,
                                        hintStyle: TextStyle(
                                            fontSize: 17,
                                            color: util.lightGrey),
                                        hintText: "Need Till Date*",
                                        fillColor: Colors.white,
                                        contentPadding: commonContentPadding,
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                          gapPadding: 0,
                                        ))),
                              ),
                            ),
                            VerticalDivider(
                              width: 10,
                            ),
                            Container(
                              height: textBoxHeight,
                              width: textBoxHeight,
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                              color: util.primaryBrown,
                            ),
                            Expanded(
                              child: SizedBox(
                                height: textBoxHeight,
                                child: TextFormField(
                                    controller: time_c,
                                    onTap: () {
                                      selecteTime(context);
                                    },
                                    style: TextStyle(
                                        backgroundColor: Colors.white),
                                    decoration: InputDecoration(
                                        filled: true,
                                        hintStyle: TextStyle(
                                            fontSize: 17,
                                            color: util.lightGrey),
                                        hintText: "Need Till Time*",
                                        fillColor: Colors.white,
                                        contentPadding: commonContentPadding,
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                          gapPadding: 0,
                                        ))),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              height: textBoxHeight,
                              width: textBoxHeight,
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                              color: util.primaryBrown,
                            ),
                            Expanded(
                              child: SizedBox(
                                height: textBoxHeight,
                                child: TextFormField(
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    keyboardType: TextInputType.number,
                                    controller: quantity,
                                    style: TextStyle(
                                        color: util.primaryBrown,
                                        backgroundColor: Colors.white,
                                        fontSize: 17),
                                    decoration: InputDecoration(
                                        hintStyle: TextStyle(
                                            fontSize: 17,
                                            color: util.lightGrey),
                                        filled: true,
                                        hintText: "Total Qty.",
                                        fillColor: Colors.white,
                                        contentPadding:
                                            EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                        ))),
                              ),
                            ),
                            VerticalDivider(
                              width: 10,
                            ),
                            Container(
                              height: textBoxHeight,
                              width: textBoxHeight,
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                              color: util.primaryBrown,
                            ),
                            Expanded(
                              child: SizedBox(
                                height: textBoxHeight,
                                child: TextFormField(
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    controller: units,
                                    style: TextStyle(
                                        backgroundColor: Colors.white),
                                    decoration: InputDecoration(
                                        filled: true,
                                        hintText: "Units",
                                        fillColor: Colors.white,
                                        hintStyle: TextStyle(
                                            fontSize: 17,
                                            color: util.lightGrey),
                                        contentPadding:
                                            EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                          gapPadding: 0,
                                        ))),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Row(
                                children: <Widget>[
                                  Container(
                                      height: textBoxHeight,
                                      width: textBoxHeight,
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.white,
                                      ),
                                      color: util.primaryBrown),
                                  Expanded(
                                    child: SizedBox(
                                      height: textBoxHeight,
                                      child: TextFormField(
                                          controller: numberOfPeople,
                                          keyboardType: TextInputType.number,
                                          style: TextStyle(
                                              backgroundColor: Colors.white),
                                          decoration: InputDecoration(
                                              filled: true,
                                              hintStyle: TextStyle(
                                                  fontSize: 17,
                                                  color: util.lightGrey),
                                              hintText: "No. of People",
                                              fillColor: Colors.white,
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      10, 0, 5, 0),
                                              border: OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                                gapPadding: 0,
                                                borderRadius:
                                                    BorderRadius.circular(0.0),
                                              ))),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(
                                          height: 20,
                                          width: 20,
                                          child: Radio(
                                              value: 1,
                                              groupValue: _radioValue,
                                              onChanged:
                                                  _handleRadioValueChange),
                                        ),
                                        Text(
                                          "Pickup",
                                          style: TextStyle(fontSize: 20),
                                        )
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(
                                          height: 20,
                                          width: 20,
                                          child: Radio(
                                              value: 2,
                                              groupValue: _radioValue,
                                              onChanged:
                                                  _handleRadioValueChange),
                                        ),
                                        Text("Drop",
                                            style: TextStyle(fontSize: 20))
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Checkbox(
                              value: submitAnonmus,
                              onChanged: (value) {
                                setState(() {
                                  submitAnonmus = value;
                                });
                              },
                            ),
                            Text(
                              "Submit Needs Anonymously...",
                              style: TextStyle(fontSize: 18),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        !submitAnonmus
                            ? Row(
                                children: <Widget>[
                                  Container(
                                    height: textBoxHeight,
                                    width: textBoxHeight,
                                    child: Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.white,
                                    ),
                                    color: util.primaryBrown,
                                  ),
                                  Expanded(
                                    child: SizedBox(
                                      height: textBoxHeight,
                                      child: TextFormField(
                                          textCapitalization:
                                              TextCapitalization.sentences,
                                          controller: first_name,
                                          style: TextStyle(
                                              color: util.primaryBrown,
                                              backgroundColor: Colors.white,
                                              fontSize: 17),
                                          decoration: InputDecoration(
                                              hintStyle: TextStyle(
                                                  fontSize: 17,
                                                  color: util.lightGrey),
                                              filled: true,
                                              hintText: "First Name",
                                              fillColor: Colors.white,
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      10, 0, 10, 0),
                                              border: OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                              ))),
                                    ),
                                  ),
                                  VerticalDivider(
                                    width: 10,
                                  ),
                                  Container(
                                    height: textBoxHeight,
                                    width: textBoxHeight,
                                    child: Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.white,
                                    ),
                                    color: util.primaryBrown,
                                  ),
                                  Expanded(
                                    child: SizedBox(
                                      height: textBoxHeight,
                                      child: TextFormField(
                                          textCapitalization:
                                              TextCapitalization.sentences,
                                          controller: last_name,
                                          style: TextStyle(
                                              backgroundColor: Colors.white),
                                          decoration: InputDecoration(
                                              filled: true,
                                              hintText: "Last Name",
                                              fillColor: Colors.white,
                                              hintStyle: TextStyle(
                                                  fontSize: 17,
                                                  color: util.lightGrey),
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      10, 0, 10, 0),
                                              border: OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                                gapPadding: 0,
                                              ))),
                                    ),
                                  ),
                                ],
                              )
                            : Container(),
                        !submitAnonmus
                            ? SizedBox(
                                height: 20,
                              )
                            : Container(),
                        !submitAnonmus
                            ? Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    height: textBoxHeight,
                                    width: textBoxHeight,
                                    child: Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.white,
                                    ),
                                    color: util.primaryBrown,
                                  ),
                                  Expanded(
                                    child: SizedBox(
                                      height: textBoxHeight + 18,
                                      child: TextFormField(
                                          maxLength: 10,
                                          controller: contact_number,
                                          keyboardType: TextInputType.number,
                                          style: TextStyle(
                                              backgroundColor: Colors.white),
                                          decoration: InputDecoration(
                                              filled: true,
                                              hintStyle: TextStyle(
                                                  fontSize: 17,
                                                  color: util.lightGrey),
                                              hintText: "Contact Number",
                                              fillColor: Colors.white,
                                              contentPadding:
                                                  commonContentPadding,
                                              border: OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                                gapPadding: 0,
                                                borderRadius:
                                                    BorderRadius.circular(3.0),
                                              ))),
                                    ),
                                  ),
                                ],
                              )
                            : Container(),
                        !submitAnonmus
                            ? SizedBox(
                                height: 20,
                              )
                            : Container(),
                        !submitAnonmus
                            ? Row(
                                children: <Widget>[
                                  Container(
                                      height: textBoxHeight,
                                      width: textBoxHeight,
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.white,
                                      ),
                                      color: util.primaryBrown),
                                  Expanded(
                                    child: SizedBox(
                                      height: textBoxHeight,
                                      child: TextFormField(
                                          textCapitalization:
                                              TextCapitalization.sentences,
                                          controller: location,
                                          style: TextStyle(
                                              backgroundColor: Colors.white),
                                          decoration: InputDecoration(
                                              filled: true,
                                              hintStyle: TextStyle(
                                                  fontSize: 17,
                                                  color: util.lightGrey),
                                              hintText:
                                                  "Door No., Floor No., Building No./Name, City, Pincode, Landmark",
                                              fillColor: Colors.white,
                                              contentPadding:
                                                  commonContentPadding,
                                              border: OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                                gapPadding: 0,
                                                borderRadius:
                                                    BorderRadius.circular(0.0),
                                              ))),
                                    ),
                                  ),
                                ],
                              )
                            : Container(),
                        !submitAnonmus
                            ? TextFormField(
                                controller: address,
                                maxLines: 2,
                                onTap: () {
                                  _locationButtonTapped();
                                },
                                readOnly: true,
                                keyboardType: TextInputType.number,
                                style: TextStyle(backgroundColor: Colors.white),
                                decoration: InputDecoration(
                                    filled: true,
                                    hintStyle: TextStyle(
                                        fontSize: 17, color: util.lightGrey),
                                    hintText:
                                        "Request Location* / Address Line 2",
                                    fillColor: Colors.white,
                                    contentPadding:
                                        EdgeInsets.fromLTRB(50, 0, 10, 0),
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide.none,
                                      gapPadding: 0,
                                      borderRadius: BorderRadius.circular(0),
                                    )))
                            : Container(),
                        !submitAnonmus
                            ? SizedBox(
                                height: 20,
                              )
                            : Container(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Checkbox(
                              value: agreeTerms,
                              onChanged: (value) {
                                setState(() {
                                  agreeTerms = value;
                                });
                              },
                            ),
                            Text("I Agree, Accept & Confirm all T&C*")
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        ButtonTheme(
                          padding: EdgeInsets.all(15),
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(125.0),
                          ),
                          minWidth: double.infinity,
                          child: RaisedButton(
                            onPressed: () async {
                              if (validate(context)) {
                                pr = new ProgressDialog(context);
                                pr.show();
                                if (!buttonClicked) {
                                  buttonClicked = true;
                                  NetworkApi.createNeedRequest(
                                          _locationData.latitude.toString(),
                                          _locationData.longitude.toString(),
                                          _selectCateogry,
                                          first_name.text,
                                          last_name.text,
                                          _selectPriorityDropDown,
                                          date_c.text,
                                          time_c.text,
                                          title.text,
                                          desc.text,
                                          contact_number.text,
                                          location.text + "," + address.text,
                                          quantity.text,
                                          units.text,
                                          numberOfPeople.text,
                                          "$_radioValue",
                                          submitAnonmus)
                                      .then((value) async {
                                    buttonClicked = false;
                                    print(jsonDecode(value.body));

                                    pr.hide().then((isHidden) {
                                      print(isHidden);
                                      var response =
                                          ApiProvider.my_response(value);
                                      print("..." +
                                          response["status"].toString());
                                      if (response["status"]) {
                                        Scaffold.of(context)
                                            .showSnackBar(SnackBar(
                                          content: Text(
                                              "Your request has been submited..."),
                                        ));
                                        Navigator.pop(
                                            mContext, {"value": true});
                                      } else {
                                        print("abc......");
                                        showSnackBar(
                                            response["message"], context);
                                      }
                                    });
                                  });
                                }
                              }
                            },
                            color: util.primaryBrown,
                            child: Text(
                              "Submit",
                              style: TextStyle(
                                fontSize: 24,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                      ],
                    ))
              ],
            )),
          ))),
    );
    throw UnimplementedError();
  }

  Future<LocationData> _getLocation() async {
    Location location = new Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return null;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return null;
      }
    }
    _locationData = await location.getLocation();
    print("location data ${_locationData.latitude},${_locationData.longitude}");
    return _locationData;
  }

  @override
  void dispose() {
    _selectPriorityDropDown = null;
    _date = null;
    _time = null;
    super.dispose();
  }

  bool validate(BuildContext c) {
    if (_selectCateogry == null) {
      showSnackBar("Select Category", c);
      return false;
    } else if (_selectPriorityDropDown == null) {
      showSnackBar("Selected Priority", c);

      return false;
    } else if (title.text.length == 0) {
      showSnackBar("Title of Need can't be empty", c);

      return false;
    } else if (desc.text.length == 0) {
      showSnackBar("Description can't be empty", c);

      return false;
    } else if (_date == null) {
      showSnackBar("Need Till can't be empty", c);

      return false;
    } else if (_time == null) {
      showSnackBar("Need Time Till can't be empty", c);

      return false;
    } else if (first_name.text.length == 0 && !submitAnonmus) {
      showSnackBar("First Name can't be empty", c);

      return false;
    } else if (last_name.text.length == 0 && !submitAnonmus) {
      showSnackBar("Last Name can't be empty", c);

      return false;
    } else if (contact_number.text.length < 10 && !submitAnonmus) {
      showSnackBar("Contact No. is invalid", c);

      return false;
    } else if (location.text.length == 0 && !submitAnonmus) {
      showSnackBar("Location can't be empty", c);

      return false;
    } else if (address.text.length == 0 && !submitAnonmus) {
      showSnackBar("Address can't be empty", c);

      return false;
    } else if (!agreeTerms) {
      showSnackBar("Please Agree Teams and Conditions", c);

      return false;
    } else {
      return true;
    }
  }

  showSnackBar(String text, BuildContext context) {
    Scaffold.of(context).showSnackBar(SnackBar(
      backgroundColor: util.urgentRed,
      duration: Duration(seconds: 1),
      content: Text(text),
    ));
  }

  onBackPress() {
    if (!checkIfAnyData()) {
      return onWillPop();
    } else {
//      return true;
      Navigator.pop(this.mContext);
    }
  }

  bool checkIfAnyData() {
    if (_selectCateogry != null) {
      return false;
    }else if(_selectGroup != null){
      return false;
    } else if (first_name.text.length != 0) {
      return false;
    } else if (last_name.text.length != 0) {
      return false;
    } else if (_selectPriorityDropDown != null) {
      return false;
    } else if (_date != null) {
      return false;
    } else if (_time != null) {
      return false;
    } else if (title.text.length != 0) {
      return false;
    } else if (desc.text.length != 0) {
      return false;
    } else if (contact_number.text.length > 0) {
      return false;
    } else if (location.text.length != 0) {
      return false;
    } else if (address.text.length != 0) {
      return false;
    } else if (quantity.text.length != 0) {
      return false;
    } else if (units.text.length != 0) {
      return false;
    } else if (numberOfPeople.text.length != 0) {
      return false;
    } else if (_radioValue != 0) {
      return false;
    } else if (agreeTerms) {
      return false;
    } else if (_selectlocation != null) {
      return false;
    } else {
      return true;
    }
  }

  DateTime currentBackPressTime;
  Future<bool> onWillPop() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Confirmation!!!'),
            content: Text(
                'Are you sure you want to go back, All of your filled data will be lost.'),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'NO',
                  style: TextStyle(color: util.primaryBrown),
                ),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              FlatButton(
                child: Text('YES', style: TextStyle(color: util.primaryBrown)),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
            ],
          );
        });
  }
}

