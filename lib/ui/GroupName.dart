import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class GroupName extends StatefulWidget {
  _GroupName createState() => _GroupName();
}

class _GroupName extends State<GroupName> with TickerProviderStateMixin {
  TabController tabController;
  var pendingcount = "";
  var approvedcount = "";
  var removedcount = "";
  var blockedcount = "";

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new DefaultTabController(
        length: 4,
        child: new Scaffold(
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.amberAccent,
            title: new Text('Group Name'),
          ),
          backgroundColor: util.primaryBrown,
          body: Column(
            children: <Widget>[
              Container(
                color: Colors.white,
                child: TabBar(
                  labelColor: Colors.black,
                  indicatorColor: Colors.yellow,
                  unselectedLabelColor: Colors.black45,
                  labelStyle: TextStyle(fontWeight: FontWeight.bold),
                  indicatorWeight: 3,
                  isScrollable: true,
                  tabs: [
                    Container(
                      height: 35,
                      child: new Tab(
                        child: Text("Pending $pendingcount"),
                      ),
                    ),
                    Container(
                      height: 35,
                      child: new Tab(
                        child: Text("Approved $approvedcount"),
                      ),
                    ),
                    Container(
                      height: 35,
                      child: new Tab(
                        child: Text(
                          "Removed $removedcount",
                        ),
                      ),
                    ),
                    Container(
                      height: 35,
                      child: new Tab(
                        child: Text(
                          "Blocked $blockedcount",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                color: Colors.brown,
                height: 2,
              ),
              Container(
                  color: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.search),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: TextField(
                            style: TextStyle(fontSize: 20),
                            decoration: InputDecoration(
                                hintStyle: TextStyle(color: util.lightGrey),
                                border: InputBorder.none,
                                hintText: "Search..."),
                          ),
                        ),
                      ],
                    ),
                  )),
              Container(
                color: Colors.black,
                height: 12,
              ),
              Container(
                alignment: Alignment.topLeft,
                height: 65,
                child: ListRequestedGroups(),
              ),
              Container(
                color: Colors.black,
                height: 12,
              ),
              Container(
                alignment: Alignment.topLeft,
                height: 65,
                child: ListRequestedGroups(),
              ),
              Container(
                color: Colors.black,
                height: 12,
              ),
              Container(
                alignment: Alignment.topLeft,
                height: 65,
                child: ListRequestedGroups(),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ListRequestedGroups extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget list = new Container(
      height: 30,
      child: new Row(children: [
        new Expanded(
          child: new Column(
            children: [
              new Expanded(
                child: new Row(children: [
                  new Padding(
                    padding: EdgeInsets.all(10.0),
                    child: new Text(
                      "Firstname",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.all(10.0),
                    child: new Text(
                      " ",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.all(10.0),
                    child: new Text(
                      "Lastname",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.all(10.0),
                    child: new Text(
                      ",",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.all(10.0),
                    child: new Text(
                      "#############",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  )
                ]),
              ),
              new Expanded(
                  child: new Row(children: [
                new Text(
                  "Location",
                  style: TextStyle(fontSize: 13),
                ),
                new Text(
                  ",",
                  style: TextStyle(fontSize: 13),
                ),
                new Text(
                  "emailid@username.com",
                  style: TextStyle(fontSize: 13),
                ),
              ]))
            ],
          ),
        )
      ]),
    );

    return new Scaffold(
      body: new ListView(
        children: [list],
      ),
    );
  }
}
