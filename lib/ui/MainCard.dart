import 'dart:async';
import 'dart:ui';

import 'package:ehelpinhands/models/NeedData.dart';
import 'package:ehelpinhands/models/NeedRequestModel.dart';
import 'package:ehelpinhands/ui/SignInScreen.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class MainCard extends StatefulWidget {
  NeedData item;
  bool isBottonVisible;
  String type;
  bool isLoggedIn;

  MainCard(this.item, this.isBottonVisible,this.type,this.isLoggedIn);



  @override
  State<StatefulWidget> createState() {
   return MainCardState();
  }

}
class MainCardState extends State<MainCard>{
  Timer _timer;
  int _start = 0;
  DateTime dateTime;
  DateTime diffDT;
  DateTime downDT;
  DateTime counterDT;
  Duration temp;
  @override
  void initState() {
    super.initState();
     dateTime = DateTime.parse(widget.item.createdAt);
     if(widget.type=="NEED") {
       downDT = DateTime.parse(widget.item.tillWhen);
//     diffDT =
//    downDT.toLocal();
       print("TILL TIME $downDT");
       print("TILL NOW ${DateTime.now()}");
       temp = Duration(milliseconds: downDT
           .toLocal()
           .millisecondsSinceEpoch - DateTime
           .now()
           .millisecondsSinceEpoch);
//    temp= downDT .difference( DateTime.now());
       print("TILL DIFFRENCE $temp");

       _start = temp.inSeconds;
//     counterDT=DateTime.fromMicrosecondsSinceEpoch(_start);

       startTimer();
     }

  }
  @override
  Widget build(BuildContext context) {
    var defaultColor=widget.item.needPriority=="Urgent - ASAP (As Soon As Possible)"||widget.item.needPriority=="Emergency"?  util.urgentRed:(widget.item.needPriority=="At the Earliest"?util.wheneverBlue:(util.earliestGreen));




    TextStyle generalTextStyleWhite =
    TextStyle(fontSize: 16, color: util.lightGrey);
    TextStyle dynamicTextStyleWhite =
    TextStyle(fontSize: 17, color: defaultColor, fontWeight: FontWeight.bold);
    TextStyle generalTextStyle = TextStyle(fontSize: 17, color: util.lightGrey);
    SizedBox sizedBox10 = SizedBox(
      width: 5,
    );
    SizedBox sizedBoxH10 = SizedBox(
      height: 3,
    );

    return Container(
      color: defaultColor,
      child:  Row(
//        crossAxisAlignment: CrossAxisAlignment.baseline,
        children: <Widget>[
          Container(width: 2,),
          //         Container(width:10,color: Colors.red,),
          Expanded(
            child: Container(
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Container(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(10, 6, 10, 3),
                      child: Row(
                        children: <Widget>[
                          Text(
                            (widget.item.needPriority=="Urgent - ASAP (As Soon As Possible)")?"Urgent - ASAP" :( (widget.item.needPriority=="Emergency")? "Emergency": widget.item.needPriority),
                            style: dynamicTextStyleWhite,overflow: TextOverflow.ellipsis,
                          ),
                          Expanded(
                              child: Center(
                                  child: Text(
                                      "${dateTime.day}/${dateTime.month}/${dateTime.year} ${dayOfweek(dateTime.weekday)}, ${dateTime.hour}:${dateTime.minute}:${dateTime.second}",
                                      style: generalTextStyleWhite))),
                          Text("${widget.item.tillWhen!=null?format(temp):"        "}"
                            ,
                              style: dynamicTextStyleWhite)
                        ],
                      ),
                    ),
                  ),
                  Container(
                    child: Container(
                      padding: EdgeInsets.fromLTRB(10, 3, 10, 3),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                  child: Text(
                                    widget.item.title,
                                    style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold,
                                        color: defaultColor),
                                  )),
                              Text(
                                  widget.item.category,
                                  style: generalTextStyleWhite)
                            ],
                          ),
                          !widget.item.isAnonymous?sizedBoxH10:Container(),
                          !widget.item.isAnonymous?Row(
                            children: <Widget>[
                              Icon(
                                Icons.my_location,
                                size: 14,
                                color: util.lightGrey,
                              ),
                              sizedBox10,
                              Expanded(
                                child: Text(
                                  widget.item.address,
                                  style: generalTextStyle,
                                ),
                              )
                            ],
                          ):Container(),
                          !widget.item.isAnonymous?sizedBoxH10:Container(),
                          !widget.item.isAnonymous?Row(
                            children: <Widget>[
                              Icon(
                                Icons.supervised_user_circle,
                                size: 14,
                                color: util.lightGrey,
                              ),
                              sizedBox10,
                              Text(
                                widget.item.firstName + " " + widget.item.lastName,
                                style: generalTextStyle,
                              ),
                              sizedBox10,
                              Text(
                                widget.item.phoneNumber,
                                style: generalTextStyle,
                              )
                            ],
                          ):Container(),
                          sizedBoxH10,
                         widget.item.totalQuantity!=null? Row(
                            children: <Widget>[
                              widget.item.totalQuantity!=null&&widget.item.totalQuantity!=""? Icon(
                                Icons.pie_chart,
                                size: 14,
                                color: util.lightGrey,
                              ):Container(),
                              widget.item.totalQuantity!=null&&widget.item.totalQuantity!=""?sizedBox10:Container(),
                              Expanded(
                                  child:widget.item.totalQuantity!=null&&widget.item.totalQuantity!=""? Text(
                                    "Total ${widget.item.totalQuantity} ${widget.item.units} ",
                                    style: generalTextStyle,
                                  ):Container()
                              ),
                              widget.item.deliveryOption!="0"? Icon(
                                Icons.directions_run,
                                size: 14,
                                color: util.lightGrey,textDirection: TextDirection.ltr,
                              ):Container(),
                              sizedBox10,
                              widget.item.deliveryOption!="0"?Text(
                                widget.item.deliveryOption!=null?widget.item.deliveryOption=="1"?"Pickup":"Drop":"",
                                style: generalTextStyle,
                              ):Container()
                            ],
                          ):Container()
                        ],
                      ),
                    ),
                  ),
                  widget.isBottonVisible
                      ? Container(
                    color: Colors.white,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(13,10,13,10),
                      child: Row(
                        children: <Widget>[
//            Expanded(child: Icon(Icons.share,color: Colors.white,),),
                          Icon(
                            Icons.message,
                            color:widget.item.hasComments?util.primaryYello:util.primaryBrown,
                          ),

//            Expanded(child: Icon(Icons.bookmark,color: Colors.white,),),
                          Expanded(
                              child:  SizedBox(height:25,width: 25,child: Image.asset("assets/images/ehh_logo_d.png"))

                          ),
                         widget.type!="REPORT"?InkWell(
                              onTap: () {
                                if(!widget.isLoggedIn){
                                  Navigator.of(context).push(
                                    new MaterialPageRoute(builder: (context) => new SignInScreen()),
//                                (Route<dynamic> route) => false,
                                  );
                                }else
                                if(widget.item.isAnonymous)
                                  Scaffold.of(context).showSnackBar(SnackBar(
                                    content:
                                    Text("Sorry This ${widget.type=="REPORT"?"Report":(widget.type=="SUGGESTION"?"Suggestion":"Need")} is Anonymous..."),
                                  ));
                              else
                                  launch("tel://${widget.item.phoneNumber}");

                              },
                              child: Icon(
                                Icons.call,
                                color:  util.primaryBrown,
                              )):Container(),
                        ],
                      ),
                    ),
                  )
                      : Container()
                ],
              ),
            ),
          ),
        ],
      ),

    );

  }
  format(Duration d) => d.toString().split('.').first.padLeft(8, "0");

  void startTimer() {
    print("init timer $_start");
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) => setState(
            () {
          if (_start < 1) {
            timer.cancel();
          } else {
            _start = _start - 1;
            temp=Duration(seconds: temp.inSeconds-1);
//            print("timer $_start");
          }
        },
      ),
    );


  }
  String dayOfweek(int i){
    switch(i){
      case 1:
        return "Mon";
      case 2:
        return "Tue";
      case 3:
        return "Wed";
      case 4:
        return "Thu";
      case 5:
        return "Fri";
      case 6:
        return "Sat";
      case 7:
        return "Sun";

    }
  }
  @override
  void dispose() {
    if(_timer!=null)
    _timer.cancel();
    super.dispose();
  }
}

class CardData {
  String type;
  String date;
  String time;
  String title;
  String location;
  String username;
  String number;
  String deliveryType;
  String quantity;
  String numberOfPeople;

  CardData(
      this.type,
      this.date,
      this.time,
      this.title,
      this.location,
      this.username,
      this.number,
      this.deliveryType,
      this.quantity,
      this.numberOfPeople);
}
