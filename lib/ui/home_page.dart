import 'package:ehelpinhands/blocs/ChuckBloc.dart';
import 'package:ehelpinhands/data/FlutterSharedPrefrence.dart';
import 'package:ehelpinhands/models/NeedData.dart';
import 'package:ehelpinhands/models/NeedRequestModel.dart';
import 'package:ehelpinhands/models/ReportRequestModel.dart';
import 'package:ehelpinhands/models/SuggestionRequestModel.dart';
import 'package:ehelpinhands/networking/Response.dart';
import 'package:ehelpinhands/ui/CategoryListScreen.dart';
import 'package:ehelpinhands/ui/CommentScreen.dart';
import 'package:ehelpinhands/ui/Error.dart';
import 'package:ehelpinhands/ui/Loading.dart';
import 'package:ehelpinhands/ui/MainCard.dart';
import 'package:ehelpinhands/ui/ROD.dart';
import 'package:ehelpinhands/ui/ReportFormScreen.dart';
import 'package:ehelpinhands/ui/SignInScreen.dart';
import 'package:ehelpinhands/ui/SuggestionFormScreen.dart';
import 'package:ehelpinhands/ui/YourProfile.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:share/share.dart';

import 'AdminSide.dart';

enum ServiceType { NEEDS, SUGGESION, REPORT }

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  ServiceType serviceType = ServiceType.NEEDS;
  var _allTabCount = "";
  var _iceTabCount = "";
  var _urgentTabCount = "";
  var _ateTabCount = "";
  var _wipTabCount = "";
  var _allSugTabCount = "";
  var _allReportTabCount = "";
  var _mySugTabCount = "";
  var _myReportTabCount = "";
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  TabController defaultTabController;
  TabController sugTabController;
  TabController reportTabController;
  ChuckBloc _bloc;
  String _selectedCategory = "All";
  String _selectedCategor2 = "All Categories";
  String _lat = "0";
  String _lng = "0";
  LocationData _location;
  String address = "";

  Future<LocationData> _getLocation() async {
    Location location = new Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return null;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return null;
      }
    }

    _locationData = await location.getLocation();
    _lat = _locationData.latitude.toString();
    _lng = _locationData.longitude.toString();
    print("location data ${_locationData.latitude},${_locationData.longitude}");
    util.getUserLocation(_locationData).then((value) {
      address =
          "${value.subLocality}, ${value.subAdminArea}, ${value.locality}, ${value.adminArea}";
    });
    return _locationData;
  }

  List<NeedData> searchData = [];
  List<NeedData> currentTabData = [];
  String searchString = "";
  String userName = "";

  @override
  void initState() {
    _bloc = ChuckBloc();
    defaultTabController = TabController(vsync: this, length: 6);
    sugTabController = TabController(vsync: this, length: 2);
    reportTabController = TabController(vsync: this, length: 1);
    defaultTabController.addListener(() {
      loadCategories();
    });
    sugTabController.addListener(() {
      loadSuggestionCategories();
    });
    reportTabController.addListener(() {
      loadReportCategories();
    });
    _getLocation().asStream().listen((event) {
      setState(() {
        setState(() {
          _location = event;
          _bloc.fetchAllNeedsList(_location.latitude.toString(),
              _location.longitude.toString(), _selectedCategor2);
        });
      });
    });

    super.initState();
  }

  loadProfile() {}

  loadGroups() {}

  loadCategories({int i = 0}) {
    if (i > 0) defaultTabController.index = i;
    switch (defaultTabController.index) {
      case 0:
        _bloc.fetchAllNeedsList(_location.latitude.toString(),
            _location.longitude.toString(), _selectedCategor2);
        break;
      case 1:
        _bloc.fetchIceNeedsList(_location.latitude.toString(),
            _location.longitude.toString(), _selectedCategor2);
        break;
      case 2:
        _bloc.fetchUrgentNeedsList(_location.latitude.toString(),
            _location.longitude.toString(), _selectedCategor2);
        break;
      case 3:
        _bloc.fetchatTheEarliestNeedsList(_location.latitude.toString(),
            _location.longitude.toString(), _selectedCategor2);
        break;
      case 4:
        _bloc.fetchWheneverNeedsList(_location.latitude.toString(),
            _location.longitude.toString(), _selectedCategor2);
        break;
      case 5:
        _bloc.fetchMyNeedsList(_location.latitude.toString(),
            _location.longitude.toString(), _selectedCategor2);
        break;
    }
  }

  loadSuggestionCategories({i = 0}) {
    if (i > 0) sugTabController.index = i;
    switch (sugTabController.index) {
      case 0:
        _bloc.fetchAllSuggestionsList(_location.latitude.toString(),
            _location.longitude.toString(), _selectedCategor2);
        break;
      case 1:
        _bloc.fetchMySuggestionsList(_location.latitude.toString(),
            _location.longitude.toString(), _selectedCategor2);
        break;
    }
  }

  loadReportCategories({i = 0}) {
    if (i > 0) reportTabController.index = i;
    switch (reportTabController.index) {
      case 0:
        _bloc.fetchMyReportsList(_location.latitude.toString(),
            _location.longitude.toString(), _selectedCategor2);
        break;
    }
  }

  var textStyle = TextStyle(fontSize: 17);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _selectedSection = "Needs";

  @override
  Widget build(BuildContext context) {
    final List<int> colorCodes = <int>[600, 500, 100];
    FlutterSharedPrefrence().getUserId().then((value) => user_id = value);
    getName().then((value) {
      userName = value;
    });

    return Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    new MaterialPageRoute(
                        builder: (context) => new YourProfile()),
                  );
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                        width: 80,
                        height: 80,
                        child: Image.asset(
                          "assets/images/ehh_logo_d.png",
                          color: util.primaryBrown,
                        )),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      userName,
                      style: TextStyle(color: util.primaryBrown, fontSize: 20),
                    ),
                  ],
                ),
              ),
              decoration: BoxDecoration(
                color: util.primaryYello,
              ),
            ),
            ListTile(
              title: Text('Dashboard', style: textStyle),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            user_id != null
                ? ListTile(
                    title: Text('Your Profile', style: textStyle),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.of(context).push(
                        new MaterialPageRoute(
                            builder: (context) => new YourProfile()),
                      );
                    },
                  )
                : Container(),
            user_id != null
                ? ListTile(
                    title: Text('Manage Groups', style: textStyle),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.of(context).push(
                        new MaterialPageRoute(
                            builder: (context) => new AdminSide()),
                      );
                    },
                  )
                : Container(),
            Divider(
              height: 10,
              color: util.primaryYello,
            ),
            ListTile(
              title: Text('Your Needs', style: textStyle),
              onTap: () {
                Navigator.pop(context);
                setState(() {
                  _selectedSection = "Needs";
                  loadCategories(i: 5);
                });
              },
            ),
            ListTile(
              title: Text('Your Suggestions', style: textStyle),
              onTap: () {
                Navigator.pop(context);
                setState(() {
                  _selectedSection = "Suggesions";
                  loadSuggestionCategories(i: 1);
                });
              },
            ),
            ListTile(
              title: Text('Your Reports', style: textStyle),
              onTap: () {
                Navigator.pop(context);
                setState(() {
                  _selectedSection = "Reports";
                  loadReportCategories(i: 1);
                });
              },
            ),
            Divider(
              height: 10,
              color: util.primaryYello,
            ),
            ListTile(
              title: Text('About', style: textStyle),
              onTap: () {
                Navigator.pop(context);
                util.launchURL("http://ehelpinhands.com");
              },
            ),
            ListTile(
              title: Text('Update App', style: textStyle),
              onTap: () {
                Navigator.pop(context);
                util.launchURL(
                    "https://play.google.com/store/apps/details?id=com.anirdesh.ehelpinhands");
              },
            ),
            ListTile(
              title: Text('Share App', style: textStyle),
              onTap: () {
                Navigator.pop(context);
                Share.share(
                    'To Meet all Your Essential Needs, Install eHelpinHands Android App:\n\n\nhttps://play.google.com/store/apps/details?id=com.anirdesh.ehelpinhands\n\n\nShare it with Your Near & Dear Ones...');
              },
            ),
            Divider(
              height: 10,
              color: util.primaryYello,
            ),
            ListTile(
              title: Text(
                'Ads',
                style: textStyle,
              ),
              onTap: () {
                var url = "http://eHelpinHands.com/Ads&Sponsor.html";
                Navigator.pop(context);
                util.launchURL(url);
              },
            ),
            ListTile(
              title: Text('Contribution', style: textStyle),
              onTap: () {
                var url = "http://eHelpinHands.com/Contribute.html";
                Navigator.pop(context);
                util.launchURL(url);
              },
            ),
            Divider(
              height: 10,
              color: util.primaryYello,
            ),
            ListTile(
              title: Text(user_id == null ? 'Sign In / Sign Up' : 'Sign Out',
                  style: textStyle),
              onTap: () {
                if (user_id != null) {
                  Navigator.pop(context);
                  setState(() {
                    FlutterSharedPrefrence().logout();
                    user_id == null;
                  });
                } else {
                  Navigator.of(context).push(
                    new MaterialPageRoute(
                        builder: (context) => new SignInScreen()),
//                                (Route<dynamic> route) => false,
                  );
                }
              },
            ),
          ],
        ),
      ),
      backgroundColor: util.primaryBrown,
      body: SafeArea(
          child: Column(
        children: <Widget>[
          Container(
            color: Colors.amberAccent,
            child: Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
              child: Row(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      _scaffoldKey.currentState.openDrawer();
                    },
                    child: Icon(
                      Icons.menu,
                      color: util.primaryBrown,
                      size: 24,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(30),
                        child: Container(
                          padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                          color: Colors.white,
                          child: Row(
                            children: <Widget>[
                              SizedBox(
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(
                                    "assets/images/ehh_logo_d.png",
                                    color: util.primaryBrown,
                                  )),
                              VerticalDivider(
                                width: 8,
                              ),
                              Container(
                                width: 1,
                                height: 30,
                                color: util.lightGrey,
                              ),
                              VerticalDivider(
                                width: 6,
                              ),
                              Expanded(
                                flex: 3,
                                child: InkWell(
                                  onTap: () {
                                    _selecteCategory();
                                  },
                                  child: Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.ac_unit,
                                        size: 16,
                                      ),
                                      VerticalDivider(
                                        width: 2,
                                      ),
                                      Text(
                                        _selectedCategor2,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: util.primaryBrown,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                width: 1,
                                height: 30,
                                color: util.lightGrey,
                              ),
                              VerticalDivider(
                                width: 10,
                              ),
                              Expanded(
                                flex: 2,
                                child: DropdownButton<String>(
                                  isExpanded: true,
                                  underline: Container(),
                                  style: TextStyle(
                                      fontSize: 16, color: util.primaryBrown),
                                  value: _selectedSection,
                                  items: <String>[
                                    'Needs',
                                    'Suggesions',
                                    'Reports',
                                  ].map((String value) {
                                    return new DropdownMenuItem<String>(
                                      value: value,
                                      child: new Text(
                                        value,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (_) {
                                    setState(() {
                                      _selectedSection = _;
                                      if (_selectedSection == "Needs") {
                                        loadCategories();
                                      } else if (_selectedSection ==
                                          "Suggesions") {
                                        loadSuggestionCategories();
                                      } else if (_selectedSection ==
                                          "Reports") {
                                        loadReportCategories();
                                      }
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        )),
                  ),
                  VerticalDivider(
                    width: 8,
                  ),
                  InkWell(
                    onTap: () {
                      FlutterSharedPrefrence().getUserId().then((value) async {
                        if (value != null) {
                          if (_selectedSection == "Needs") {
                            print("NEEDS");
//                          var response=
                            Map results = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        RequestOnDemand(address, _location)));
                            if (results != null) {
                              loadCategories();
                            }
//                            response.then((value) =>{
//
//                                if (value != null) {
//                                    loadCategories()
//                                  }
//                            });
                          } else if (_selectedSection == "Suggesions") {
                            print("SUGGESION");
                            Map results = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SuggestionFormScreen(
                                        address, _location)));
                            if (results != null) {
                              loadSuggestionCategories();
                            }
                          } else if (_selectedSection == "Reports") {
                            print("REPORT");
                            Map results = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ReportFormScreen(address, _location)));
                            if (results != null) {
                              loadReportCategories();
                            }
                          }
                        } else {
                          Navigator.of(context).push(
                            new MaterialPageRoute(
                                builder: (context) => new SignInScreen()),
//                                (Route<dynamic> route) => false,
                          );
                        }
                      });
                    },
                    child: Padding(
                      padding: EdgeInsets.all(3),
                      child: SizedBox(
                          width: 24,
                          height: 24,
                          child: Image.asset(
                            "assets/images/addition_plus_sign.png",
                            color: util.primaryBrown,
                          )),
                    ),
                  )
                ],
              ),
            ),
          ),
          if (_selectedSection == "Needs")
            Container(
              color: Colors.white,
              child: TabBar(
                controller: defaultTabController,
                indicatorWeight: 3,
                labelColor: Colors.black,
                labelStyle: TextStyle(fontWeight: FontWeight.bold),
                unselectedLabelColor: Colors.black45,
                indicatorColor: Colors.yellow,
                isScrollable: true,
                tabs: [
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text(
                        "All $_allTabCount",
                      ),
                    ),
                  ),
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text(
                        "Emergency $_iceTabCount",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ),
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text(
                        "Urgent - ASAP $_urgentTabCount",
                      ),
                    ),
                  ),
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text("At the Earliest $_ateTabCount"),
                    ),
                  ),
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text("Whenever it's Possible $_wipTabCount"),
                    ),
                  ),
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text("My Needs $_wipTabCount"),
                    ),
                  ),
                ],
              ),
            ),
          if (_selectedSection == "Suggesions")
            Container(
              color: Colors.white,
              child: TabBar(
                controller: sugTabController,
                indicatorWeight: 3,
                labelColor: Colors.black,
                labelStyle: TextStyle(fontWeight: FontWeight.bold),
                unselectedLabelColor: Colors.black45,
                indicatorColor: Colors.yellow,
                isScrollable: false,
                tabs: [
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text("All $_allSugTabCount"),
                    ),
                  ),
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text(
                        "My Suggesions $_mySugTabCount",
                      ),
                    ),
                  ),
                ],
              ),
            ),
          if (_selectedSection == "Reports")
            Container(
              color: Colors.white,
              child: TabBar(
                controller: reportTabController,
                indicatorWeight: 3,
                labelColor: Colors.black,
                labelStyle: TextStyle(fontWeight: FontWeight.bold),
                unselectedLabelColor: Colors.black45,
                indicatorColor: Colors.yellow,
                isScrollable: false,
                tabs: [
                  Container(
                    height: 35,
                    child: new Tab(
                      child: Text(
                        "My Reports $_myReportTabCount",
                      ),
                    ),
                  ),
                ],
              ),
            ),
          Container(
            color: Colors.brown,
            height: 2,
          ),
          Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.search),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: TextField(
                        onChanged: onSearchTextChanged,
                        style: TextStyle(fontSize: 20),
                        decoration: InputDecoration(
                            hintStyle: TextStyle(color: util.lightGrey),
                            border: InputBorder.none,
                            hintText: "Search..."),
                      ),
                    ),
//                        Icon(Icons.location_on),
//                  SizedBox(
//                    width: 10,
//                  ),
//                  Icon(Icons.sort_by_alpha),
//                  SizedBox(
//                    width: 10,
//                  ),
//                  Icon(Icons.filter_list)
                  ],
                ),
              )),
          Container(
            height: 8,
          ),
          if (_location != null)
            Expanded(
                child: _selectedSection == "Needs"
                    ? TabBarView(
                        controller: defaultTabController,
                        children: [
                          StreamBuilder<Response<NeedRequestModel>>(
                              stream:
                                  _bloc.allNeedDataStream.asBroadcastStream(),
                              builder: (context, snapshot) {
                                if (snapshot?.data?.data != null) {
                                  _allTabCount =
                                      "(${snapshot.data.data.data.length})";
                                  print("All Tab count $_allTabCount");
                                  currentTabData = snapshot.data.data.data;
                                }
                                return Container(
                                  color: util.primaryBrown,
                                  child: SingleTabWidget(snapshot, searchData,
                                      searchString, user_id != null),
                                );
                              }),
                          StreamBuilder<Response<NeedRequestModel>>(
                              stream: _bloc.icetDataStream,
                              builder: (context, snapshot) {
                                if (snapshot?.data?.data != null) {
                                  _iceTabCount =
                                      "(${snapshot.data.data.data.length})";
                                  currentTabData = snapshot.data.data.data;
                                }
                                return Container(
                                  color: util.primaryBrown,
                                  child: SingleTabWidget(snapshot, searchData,
                                      this.searchString, user_id != null),
                                );
                              }),
                          StreamBuilder<Response<NeedRequestModel>>(
                              stream: _bloc.urgentDataStream,
                              builder: (context, snapshot) {
                                if (snapshot?.data?.data != null) {
                                  print("not emptry");
                                  currentTabData = snapshot.data.data.data;
                                  _urgentTabCount =
                                      "(${snapshot.data.data.data.length})";
                                }
                                return Container(
                                  color: util.primaryBrown,
                                  child: SingleTabWidget(snapshot, searchData,
                                      this.searchString, user_id != null),
                                );
                              }),
                          StreamBuilder<Response<NeedRequestModel>>(
                              stream: _bloc.atTheEarliestDataStream,
                              builder: (context, snapshot) {
                                if (snapshot?.data?.data != null) {
                                  currentTabData = snapshot.data.data.data;
                                }
                                return Container(
                                  color: util.primaryBrown,
                                  child: SingleTabWidget(snapshot, searchData,
                                      this.searchString, user_id != null),
                                );
                              }),
                          StreamBuilder<Response<NeedRequestModel>>(
                              stream: _bloc.wheneverDataStream,
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  if (snapshot?.data?.data != null &&
                                      !snapshot.data.data.data.isEmpty) {
                                    currentTabData = snapshot.data.data.data;
                                    return Container(
                                      color: util.primaryBrown,
                                      child: SingleTabWidget(
                                          snapshot,
                                          searchData,
                                          this.searchString,
                                          user_id != null),
                                    );
                                  } else {
                                    return EmptyDATA("Needs");
                                  }
                                } else
                                  return EmptyDATA("Needs");
                              }),
                          StreamBuilder<Response<NeedRequestModel>>(
                              stream: _bloc.myNeedDataStream,
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  if (snapshot?.data?.data != null &&
                                      !snapshot.data.data.data.isEmpty) {
                                    currentTabData = snapshot.data.data.data;
                                    return Container(
                                      color: util.primaryBrown,
                                      child: SingleTabWidget(
                                          snapshot,
                                          searchData,
                                          this.searchString,
                                          user_id != null),
                                    );
                                  } else {
                                    return EmptyDATA("Needs");
                                  }
                                } else
                                  return EmptyDATA("Needs");
                              }),
                        ],
                      )
                    : _selectedSection == "Suggesions"
                        ? TabBarView(
                            controller: sugTabController,
                            children: [
                              StreamBuilder<Response<SuggestionRequestModel>>(
                                  stream: _bloc.allSuggestionStream,
                                  builder: (context, snapshot) {
                                    if (snapshot?.data?.data != null) {
                                      _allSugTabCount =
                                          "(${snapshot.data.data.data.length})";
                                      currentTabData = snapshot.data.data.data;
                                    }
                                    return Container(
                                      color: util.primaryBrown,
                                      child: SingleTabWidgetSuggestion(
                                          snapshot,
                                          searchData,
                                          searchString,
                                          user_id != null),
                                    );
                                  }),
                              StreamBuilder<Response<SuggestionRequestModel>>(
                                  stream: _bloc.mySuggestionStream,
                                  builder: (context, snapshot) {
                                    if (snapshot?.data?.data != null) {
                                      _mySugTabCount =
                                          "(${snapshot.data.data.data.length})";
                                      currentTabData = snapshot.data.data.data;
                                    }
                                    return Container(
                                      color: util.primaryBrown,
                                      child: SingleTabWidgetSuggestion(
                                          snapshot,
                                          searchData,
                                          this.searchString,
                                          user_id != null),
                                    );
                                  }),
                            ],
                          )
                        : TabBarView(
                            controller: reportTabController,
                            children: [
//                              StreamBuilder<Response<ReportRequestModel>>(
//                                  stream: _bloc.allReportStream,
//                                  builder: (context, snapshot) {
//                                    if (snapshot?.data?.data != null) {
//                                      _allTabCount = "(${snapshot.data.data.data.length})";
//                                      currentTabData = snapshot.data.data.data;
//                                    }
//                                    return Container(
//                                      color: util.primaryBrown,
//                                      child: SingleTabWidgetReport(snapshot, searchData, searchString),
//                                    );
//                                  }),
                              StreamBuilder<Response<ReportRequestModel>>(
                                  stream: _bloc.myReportStream,
                                  builder: (context, snapshot) {
                                    if (snapshot?.data?.data != null) {
                                      _myReportTabCount =
                                          "(${snapshot.data.data.data.length})";
                                      currentTabData = snapshot.data.data.data;
                                    }
                                    return Container(
                                      color: util.primaryBrown,
                                      child: SingleTabWidgetReport(
                                          snapshot,
                                          searchData,
                                          this.searchString,
                                          user_id != null),
                                    );
                                  }),
                            ],
                          ))
          else
            Expanded(
                child: Center(
                    child: Container(
              child: Text(
                "Fetching Location....",
                style: TextStyle(color: Colors.white),
              ),
            )))
        ],
      )),
    );

    throw UnimplementedError();
  }

  Future<String> getName() async {
    return await FlutterSharedPrefrence().getName();
  }

  Future _selecteCategory() async {
    var results = await Navigator.of(context).push(MaterialPageRoute<dynamic>(
        builder: (context) => CategoryListScreen(true)));

    if (results != null && results.containsKey('selected')) {
      setState(() {
        print("selected " + results['selected']);
        _selectedCategor2 = results['selected'];

        if (_selectedSection == "Needs") {
          loadCategories();
        } else if (_selectedSection == "Suggesions") {
          loadSuggestionCategories();
        } else if (_selectedSection == "Reports") {
          loadReportCategories();
        }
      });
    }
  }

  String user_id = null;

  @override
  bool get wantKeepAlive => true;

  onSearchTextChanged(String text) async {
    print("changed -$text-");
    searchString = text;
    searchData.clear();
    if (text.isEmpty) {
      searchData.addAll(currentTabData);
      setState(() {});
      return;
    }

    currentTabData.forEach((item) {
      if (item.title.toLowerCase().contains(text.toLowerCase()) ||
          item.firstName.toLowerCase() == text.toLowerCase()) {
        searchData.add(item);
        print("matched $item  --- $text");
      }
    });

    setState(() {});
  }
}

class SingleTabWidget extends StatelessWidget {
  AsyncSnapshot<Response<NeedRequestModel>> data;
  List<NeedData> searchData;
  String searchString;
  bool isLoggedIn;

  SingleTabWidget(
      this.data, this.searchData, this.searchString, this.isLoggedIn);

  @override
  Widget build(BuildContext context) {
    if (data.hasData) {
      switch (data.data.status) {
        case Status.COMPLETED:
          return (!data.data.data.data.isEmpty)
              ? ListView.separated(
                  key: new PageStorageKey(data),
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(
                        height: 8,
                      ),
                  itemCount: searchString.length > 0
                      ? searchData.length
                      : data.data.data.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                        onTap: () {
                          FlutterSharedPrefrence().getUserId().then((value) {
                            if (value != null) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CommentScreen(
                                          searchString.length > 0
                                              ? searchData[index]
                                              : data.data.data.data[index],
                                          "NEED")));
                            } else {
                              Navigator.of(context).push(
                                new MaterialPageRoute(
                                    builder: (context) => new SignInScreen()),
//                                (Route<dynamic> route) => false,
                              );
                            }
                          });
                        },
                        child: MainCard(
                            searchString.length > 0
                                ? searchData[index]
                                : data.data.data.data[index],
                            true,
                            "NEED",
                            isLoggedIn));
                  })
              : EmptyDATA("Needs");
          break;
        case Status.LOADING:
          return Loading(loadingMessage: data.data.message);
          break;
        case Status.ERROR:
          return Error(
            errorMessage: "Some error occured",
            onRetryPressed: () {},
          );
          break;
        case Status.EMPTY_DATA:
          return EmptyDATA("Needs");
      }
    } else {
      return Loading(
        loadingMessage: "Loading...",
      );
    }
  }
}

class SingleTabWidgetSuggestion extends StatelessWidget {
  AsyncSnapshot<Response<SuggestionRequestModel>> data;
  List<NeedData> searchData;
  String searchString;
  bool isLoggedIn;

  SingleTabWidgetSuggestion(
      this.data, this.searchData, this.searchString, this.isLoggedIn);

  @override
  Widget build(BuildContext context) {
    if (data.hasData) {
      switch (data.data.status) {
        case Status.COMPLETED:
          return (!data.data.data.data.isEmpty)
              ? ListView.separated(
                  key: new PageStorageKey(data),
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(
                        height: 8,
                      ),
                  itemCount: searchString.length > 0
                      ? searchData.length
                      : data.data.data.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                        onTap: () {
                          FlutterSharedPrefrence().getUserId().then((value) {
                            if (value != null) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => CommentScreen(
                                        searchString.length > 0
                                            ? searchData[index]
                                            : data.data.data.data[index],
                                        "SUGGESTION"),
                                  ));
                            } else {
                              Navigator.of(context).push(
                                new MaterialPageRoute(
                                    builder: (context) => new SignInScreen()),
//                                (Route<dynamic> route) => false,
                              );
                            }
                          });
                        },
                        child: MainCard(
                            searchString.length > 0
                                ? searchData[index]
                                : data.data.data.data[index],
                            true,
                            "SUGGESTION",
                            isLoggedIn));
                  })
              : EmptyDATA("Suggestions");
          break;
        case Status.LOADING:
          return Loading(loadingMessage: data.data.message);
          break;
        case Status.ERROR:
          return Error(
            errorMessage: "Some error occured",
            onRetryPressed: () {},
          );
          break;
        case Status.EMPTY_DATA:
          return EmptyDATA("Suggestions");
      }
    } else {
      return Loading(
        loadingMessage: "Loading...",
      );
    }
  }
}

class SingleTabWidgetReport extends StatelessWidget {
  AsyncSnapshot<Response<ReportRequestModel>> data;
  List<NeedData> searchData;
  String searchString;
  bool isLoggedIn;

  SingleTabWidgetReport(
      this.data, this.searchData, this.searchString, this.isLoggedIn);

  @override
  Widget build(BuildContext context) {
    if (data.hasData) {
      switch (data.data.status) {
        case Status.COMPLETED:
          return (!data.data.data.data.isEmpty)
              ? ListView.separated(
                  key: new PageStorageKey(data),
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(
                        height: 8,
                      ),
                  itemCount: searchString.length > 0
                      ? searchData.length
                      : data.data.data.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                        onTap: () {
                          FlutterSharedPrefrence().getUserId().then((value) {
                            if (value != null) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CommentScreen(
                                          searchString.length > 0
                                              ? searchData[index]
                                              : data.data.data.data[index],
                                          "REPORT")));
                            } else {
                              Navigator.of(context).push(
                                new MaterialPageRoute(
                                    builder: (context) => new SignInScreen()),
//                                (Route<dynamic> route) => false,
                              );
                            }
                          });
                        },
                        child: MainCard(
                            searchString.length > 0
                                ? searchData[index]
                                : data.data.data.data[index],
                            true,
                            "REPORT",
                            isLoggedIn));
                  })
              : EmptyDATA("Report");
          break;
        case Status.LOADING:
          return Loading(loadingMessage: data.data.message);
          break;
        case Status.ERROR:
          return Error(
            errorMessage: "Some error occured",
            onRetryPressed: () {},
          );
          break;
        case Status.EMPTY_DATA:
          return EmptyDATA("Report");
      }
    } else {
      return Loading(
        loadingMessage: "Loading...",
      );
    }
  }
}

class EmptyDATA extends StatelessWidget {
  String text;

  EmptyDATA(this.text);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
              height: 120,
              width: 120,
              child: Image.asset(
                "assets/images/addition_plus_sign.png",
                color: Color(0xFF473801),
              )),
          SizedBox(height: 20),
          Text(
            "Add Your $text",
            style: TextStyle(color: Color(0xFF473801), fontSize: 30),
          )
        ],
      ),
    );
  }
}
