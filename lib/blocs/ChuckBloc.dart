import 'dart:async';

import 'package:ehelpinhands/data/FlutterSharedPrefrence.dart';
import 'package:ehelpinhands/models/NeedRequestModel.dart';
import 'package:ehelpinhands/models/ReportRequestModel.dart';
import 'package:ehelpinhands/models/SuggestionRequestModel.dart';
import 'package:ehelpinhands/models/checkResponse.dart';
import 'package:ehelpinhands/networking/Response.dart';
import 'package:ehelpinhands/repository/ChuckRepository.dart';
import 'package:ehelpinhands/repository/CustomRequestRepository.dart';
import 'package:ehelpinhands/ui/SuggestionFormScreen.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChuckBloc {
  ChuckRepository _chuckRepository = ChuckRepository();

  StreamController _allNeedRequestDataController;
  StreamController _allSuggestionRequestController;
  StreamController _mySuggestionRequestController;
  StreamController _allReportRequestController;
  StreamController _myReportRequestController;

  StreamSink<Response<NeedRequestModel>> get allNeedDataSink =>
      _allNeedRequestDataController.sink;

  Stream<Response<NeedRequestModel>> get allNeedDataStream =>
      _allNeedRequestDataController.stream;

  StreamSink<Response<NeedRequestModel>> get myNeedDataSink =>
      _allNeedRequestDataController.sink;

  Stream<Response<NeedRequestModel>> get myNeedDataStream =>
      _allNeedRequestDataController.stream;

  StreamSink<Response<NeedRequestModel>> get iceDataSink =>
      _allNeedRequestDataController.sink;

  Stream<Response<NeedRequestModel>> get icetDataStream =>
      _allNeedRequestDataController.stream;

  StreamSink<Response<NeedRequestModel>> get urgentDataSink =>
      _allNeedRequestDataController.sink;

  Stream<Response<NeedRequestModel>> get urgentDataStream =>
      _allNeedRequestDataController.stream;

  StreamSink<Response<NeedRequestModel>> get atTheEarliestDataSink =>
      _allNeedRequestDataController.sink;

  Stream<Response<NeedRequestModel>> get atTheEarliestDataStream =>
      _allNeedRequestDataController.stream;

  StreamSink<Response<NeedRequestModel>> get wheneverDataSink =>
      _allNeedRequestDataController.sink;

  Stream<Response<NeedRequestModel>> get wheneverDataStream =>
      _allNeedRequestDataController.stream;


  StreamSink<Response<SuggestionRequestModel>> get mySuggestionSink =>
      _mySuggestionRequestController.sink;
  Stream<Response<SuggestionRequestModel>> get mySuggestionStream =>
      _mySuggestionRequestController.stream;


  Stream<Response<SuggestionRequestModel>> get allSuggestionStream =>
      _allSuggestionRequestController.stream;
  StreamSink<Response<SuggestionRequestModel>> get allSuggestionSink =>
      _allSuggestionRequestController.sink;

  Stream<Response<ReportRequestModel>> get myReportStream =>
      _myReportRequestController.stream;
  StreamSink<Response<ReportRequestModel>> get myReportSink =>
      _myReportRequestController.sink;

  Stream<Response<ReportRequestModel>> get allReportStream =>
      _allReportRequestController.stream;
  StreamSink<Response<ReportRequestModel>> get allReportSink =>
      _allReportRequestController.sink;

  ChuckBloc() {
    _allNeedRequestDataController =
        StreamController<Response<NeedRequestModel>>.broadcast();
    _allNeedRequestDataController =
        StreamController<Response<NeedRequestModel>>.broadcast();
    _mySuggestionRequestController =
        StreamController<Response<SuggestionRequestModel>>.broadcast();
    _allSuggestionRequestController =
        StreamController<Response<SuggestionRequestModel>>.broadcast();
    _myReportRequestController =
        StreamController<Response<ReportRequestModel>>.broadcast();
    _allReportRequestController =
        StreamController<Response<ReportRequestModel>>.broadcast();
  }

//  fetchChuckyJoke(String category) async {
//    chuckDataSink.add(Response.loading('Getting A Chucky Joke!'));
//    try {
//      chuckResponse chuckJoke = await _chuckRepository.fetchChuckJoke(category);
//      chuckDataSink.add(Response.completed(chuckJoke));
//    } catch (e) {
//      chuckDataSink.add(Response.error(e.toString()));
//      print(e);
//    }
//  }



  fetchMyNeedsList(String lat, String lng,String cat2) async {
    myNeedDataSink.add(Response.loading('Loading...'));
      try {
        NeedRequestModel needRequestModel =
            await _chuckRepository.fetchMyNeedList(await FlutterSharedPrefrence().getUserId());
  //          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
  //      allNeedDataSink.add(Response.completed(needRequestModel));
        if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
          myNeedDataSink.add(Response.completed(needRequestModel));
        else
          myNeedDataSink.add(Response.emptyData());
      } catch (e) {
        myNeedDataSink.add(Response.error(e.toString()));
        print(e);
      }
  }
  fetchAllNeedsList(String lat, String lng,String cat2) async {
    allNeedDataSink.add(Response.loading('Loading...'));
      try {
        NeedRequestModel needRequestModel =
            await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), "All", lat, lng,cat2);
  //          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
  //      allNeedDataSink.add(Response.completed(needRequestModel));
        if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
          allNeedDataSink.add(Response.completed(needRequestModel));
        else
          allNeedDataSink.add(Response.emptyData());
      } catch (e) {
        allNeedDataSink.add(Response.error(e.toString()));
        print(e);
      }
  }


  fetchMySuggestionsList(String lat, String lng,String cat2) async {
    mySuggestionSink.add(Response.loading('Loading...'));
      try {
        SuggestionRequestModel needRequestModel =
            await _chuckRepository.fetchMySuggestionList(await FlutterSharedPrefrence().getUserId());
  //          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
  //      allNeedDataSink.add(Response.completed(needRequestModel));
        if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
          mySuggestionSink.add(Response.completed(needRequestModel));
        else
          mySuggestionSink.add(Response.emptyData());
      } catch (e) {
        mySuggestionSink.add(Response.error(e.toString()));
        print(e);
      }
  }
  fetchAllSuggestionsList(String lat, String lng,String cat2) async {
    allSuggestionSink.add(Response.loading('Loading...'));
      try {
        SuggestionRequestModel needRequestModel =
            await _chuckRepository.fetchAllSuggestionList(await FlutterSharedPrefrence().getUserId(), "All", lat, lng,cat2);
  //          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
  //      allNeedDataSink.add(Response.completed(needRequestModel));
        if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
          allSuggestionSink.add(Response.completed(needRequestModel));
        else
          allSuggestionSink.add(Response.emptyData());
      } catch (e) {
        allSuggestionSink.add(Response.error(e.toString()));
        print(e);
      }
  }


  fetchMyReportsList(String lat, String lng,String cat2) async {
    myReportSink.add(Response.loading('Loading...'));
      try {
        ReportRequestModel needRequestModel =
            await _chuckRepository.fetchMyReportList(await FlutterSharedPrefrence().getUserId());
  //          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
  //      allNeedDataSink.add(Response.completed(needRequestModel));
        if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
          myReportSink.add(Response.completed(needRequestModel));
        else
          myReportSink.add(Response.emptyData());
      } catch (e) {
        myReportSink.add(Response.error(e.toString()));
        print(e);
      }
  }
  fetchAllReportsList(String lat, String lng,String cat2) async {
    allReportSink.add(Response.loading('Loading...'));
      try {
        ReportRequestModel needRequestModel =
            await _chuckRepository.fetchAllReportList(await FlutterSharedPrefrence().getUserId(), "All", lat, lng,cat2);
  //          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
  //      allNeedDataSink.add(Response.completed(needRequestModel));
        if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
          allReportSink.add(Response.completed(needRequestModel));
        else
          allReportSink.add(Response.emptyData());
      } catch (e) {
        myReportSink.add(Response.error(e.toString()));
        print(e);
      }
  }

  fetchIceNeedsList(String lat, String lng,String cat2) async {
    iceDataSink.add(Response.loading('Loading...'));
    try {
      NeedRequestModel needRequestModel =
          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), "Emergency", lat, lng,cat2);
//          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
//      iceDataSink.add(Response.completed(needRequestModel));
      if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
        iceDataSink.add(Response.completed(needRequestModel));
      else
        iceDataSink.add(Response.emptyData());
    } catch (e) {
      iceDataSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  fetchUrgentNeedsList(String lat, String lng,String cat2) async {
    urgentDataSink.add(Response.loading('Loading...'));
    try {
      NeedRequestModel needRequestModel =
          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), "Urgent - ASAP (As Soon As Possible)", lat, lng,cat2);
//          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
//      urgentDataSink.add(Response.completed(needRequestModel));
      if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
        urgentDataSink.add(Response.completed(needRequestModel));
      else
        urgentDataSink.add(Response.emptyData());
    } catch (e) {
      urgentDataSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  fetchatTheEarliestNeedsList(String lat, String lng,String cat2) async {
    atTheEarliestDataSink.add(Response.loading('Loading...'));
    try {
      NeedRequestModel needRequestModel = await _chuckRepository.fetchNeedsList(
          await FlutterSharedPrefrence().getUserId(), "At the Earliest", lat, lng,cat2);
//          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
//      atTheEarliestDataSink.add(Response.loading("Loading..."));
      if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
        atTheEarliestDataSink.add(Response.completed(needRequestModel));
//      else
//        atTheEarliestDataSink.add(Response.emptyData());
    } catch (e) {
      atTheEarliestDataSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  fetchWheneverNeedsList(String lat, String lng,String cat2) async {
    wheneverDataSink.add(Response.loading('Loading...'));
    try {
      NeedRequestModel needRequestModel = await _chuckRepository.fetchNeedsList(
          await FlutterSharedPrefrence().getUserId(), "Whenever it's possible", lat, lng,cat2);
      if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
        wheneverDataSink.add(Response.completed(needRequestModel));
      else
        wheneverDataSink.add(Response.emptyData());
    } catch (e) {
      wheneverDataSink.add(Response.emptyData());
      print(e);
    }
  }

  dispose() {
    _allNeedRequestDataController?.close();
  }
}
