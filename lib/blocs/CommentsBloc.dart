import 'dart:async';

import 'package:ehelpinhands/data/FlutterSharedPrefrence.dart';
import 'package:ehelpinhands/models/NeedCommentsModel.dart';
import 'package:ehelpinhands/networking/Response.dart';
import 'package:ehelpinhands/repository/CommentRepository.dart';

class CommentBloc{

  String need_id;
  String type;
  CommentRepository _commentRepository=CommentRepository();

  StreamController _needCommentsDataController;

  StreamSink<Response<NeedCommentsModel>> get allCommentsDataSink =>
      _needCommentsDataController.sink;
  Stream<Response<NeedCommentsModel>> get allCommentsDataStream =>
      _needCommentsDataController.stream;

CommentBloc(this.need_id,this.type){
  _needCommentsDataController=StreamController<Response<NeedCommentsModel>>.broadcast();
  fetchAllCommentsList();
}

  fetchAllCommentsList() async {
    allCommentsDataSink.add(Response.loading('Loading data...'));
    try {
      NeedCommentsModel needRequestModel =
      await _commentRepository.fetchCommentList(await FlutterSharedPrefrence().getUserId(),need_id,type);
//          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
      allCommentsDataSink.add(Response.completed(needRequestModel));
    } catch (e) {
      allCommentsDataSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  postComment(String message) async{
    try {

     FlutterSharedPrefrence().getUserId().then((value)async {
       NeedCommentsModel needRequestModel =
          await  _commentRepository.postComments(message,value,need_id);
//          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
       allCommentsDataSink.add(Response.completed(needRequestModel));
      });

    } catch (e) {
      allCommentsDataSink.add(Response.error(e.toString()));
      print(e);
    }
  }
}