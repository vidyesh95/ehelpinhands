import 'dart:async';

import 'package:ehelpinhands/data/FlutterSharedPrefrence.dart';
import 'package:ehelpinhands/models/NeedRequestModel.dart';
import 'package:ehelpinhands/networking/Response.dart';
import 'package:ehelpinhands/repository/YourDataRepo.dart';

class MyDataBloc{
  YourDataRepo _yourDataRepo=YourDataRepo();
  FlutterSharedPrefrence sharedPrefrence=FlutterSharedPrefrence();
  StreamController _youNeedsController;
  StreamController _youSuggestionController;
  StreamController _youReportsController;

  StreamSink<Response<NeedRequestModel>> get myNeedRequest=>_youNeedsController.sink;
  StreamSink<Response<NeedRequestModel>> get mySuggestionRequest=>_youSuggestionController.sink;
  StreamSink<Response<NeedRequestModel>> get myReportsRequest=>_youReportsController.sink;

  MyDataBloc(){
    _youNeedsController=StreamController<Response<NeedRequestModel>>.broadcast();
    _youSuggestionController=StreamController<Response<NeedRequestModel>>.broadcast();
    _youReportsController=StreamController<Response<NeedRequestModel>>.broadcast();
  }


  getYouNeeds() async {
    _youNeedsController.add(Response.loading('Loading...'));
    try {
      NeedRequestModel needRequestModel =
          await _yourDataRepo.fetchYourNeedsList(await FlutterSharedPrefrence().getUserId());
//          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
//      allNeedDataSink.add(Response.completed(needRequestModel));
    if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
      _youNeedsController.add(Response.completed(needRequestModel));
    else
      _youNeedsController.add(Response.emptyData());
    } catch (e) {
      _youNeedsController.add(Response.error(e.toString()));
    print(e);
    }
  }
  getYourSuggestions() async {
    _youSuggestionController.add(Response.loading('Loading...'));
    try {
      NeedRequestModel needRequestModel =
          await _yourDataRepo.fetchYourNeedsList(await FlutterSharedPrefrence().getUserId());
//          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
//      allNeedDataSink.add(Response.completed(needRequestModel));
    if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
      _youSuggestionController.add(Response.completed(needRequestModel));
    else
      _youSuggestionController.add(Response.emptyData());
    } catch (e) {
      _youSuggestionController.add(Response.error(e.toString()));
    print(e);
    }
  }
  getYourReports() async {
    _youReportsController.add(Response.loading('Loading...'));
    try {
      NeedRequestModel needRequestModel =
          await _yourDataRepo.fetchYourNeedsList(await FlutterSharedPrefrence().getUserId());
//          await _chuckRepository.fetchNeedsList(await FlutterSharedPrefrence().getUserId(), category, lat, lng);
//      allNeedDataSink.add(Response.completed(needRequestModel));
    if (needRequestModel.data != null || !needRequestModel.data.isEmpty)
      _youReportsController.add(Response.completed(needRequestModel));
    else
      _youReportsController.add(Response.emptyData());
    } catch (e) {
      _youReportsController.add(Response.error(e.toString()));
    print(e);
    }
  }

}