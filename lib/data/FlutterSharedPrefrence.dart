import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class  FlutterSharedPrefrence {
  setFcmId(String fcmId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
     prefs.setString("FCM_ID",fcmId);
  }

  Future<String> getFcmId() async {
    SharedPreferences prefs = await SharedPreferences.  getInstance();
    return prefs.getString("FCM_ID");
  }

  Future<String> getUserId()async {
    final SharedPreferences pref = await SharedPreferences.getInstance();


//    .then((value) {
      return pref.getString("USER_ID")??null;
//    });

  }
  Future<String> getName()async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    try {
      return pref.getString("FIRST_NAME") + " " + pref.getString("LAST_NAME") ?? null;
    }catch(e){
      return "Guest";
    }
  }
  Future<String> getLName()async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    try {
      return pref.getString("LAST_NAME") ?? null;
    }catch(e){
      return "";
    }
  }

  Future<String> getFName()async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    try {
      return pref.getString("FIRST_NAME");
    }catch(e){
      return "";
    }
  }
  Future<String> getPhone()async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    try {
      return pref.getString("PHONE");
    }catch(e){
      return "";
    }
  }
  Future<String> getEmail()async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    try {
      return pref.getString("EMAIL");
    }catch(e){
      return "";
    }
  }
  Future<String> getGender()async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    try {
      return pref.getString("GENDER");
    }catch(e){
      return "";
    }
  }
  Future<String> getDob()async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    try {
      return pref.getString("DOB");
    }catch(e){
      return "";
    }
  }

  setUserData(
      String id,
      String firstName,
      String lastName,
      String email,
      String phone,
      String deviceName,
      String lat,
      String lng,
      String gender,
      String dob,
      bool isBlocked) async {
   SharedPreferences.getInstance().then((prefs){
     print("SET ID $id");
     prefs.setString("USER_ID", id);
     prefs.setString("FIRST_NAME", firstName);
     prefs.setString("LAST_NAME", lastName);
     prefs.setString("EMAIL", email);
     prefs.setString("PHONE", phone);
     prefs.setString("DEVICE_NAME", deviceName);
     prefs.setString("LAT", lat);
     prefs.setString("LNG", lng);
     prefs.setString("GENDER", gender);
     prefs.setString("DOB", dob);
//     prefs.setString("FCM_ID", fcmId);
     prefs.setBool("IS_BLOCKED", isBlocked);
   });


//    return prefs.getString("USER_ID");
  }

  logout(){
    SharedPreferences.getInstance().then((prefs){
      prefs.remove("USER_ID");
      prefs.remove("FIRST_NAME");
      prefs.remove("LAST_NAME");
      prefs.remove("EMAIL");
      prefs.remove("PHONE");
      prefs.remove("DEVICE_NAME");
      prefs.remove("LAT");
      prefs.remove("LNG");
      prefs.remove("IS_BLOCKED");
      GoogleSignIn googleSignIn = GoogleSignIn();
      googleSignIn.signOut();
    });
  }

//  Future<String> setUserLoggedIn()
}
