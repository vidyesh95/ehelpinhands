import 'dart:convert';

import 'package:ehelpinhands/data/FlutterSharedPrefrence.dart';
import 'package:ehelpinhands/networking/ApiProvider.dart';
import 'package:http/http.dart' as http;


class NetworkApi{

 static Future<http.Response> createNeedRequest(String lat,String lng,String _selectCategoryDropdown,String first_name,String last_name,String _selectPriorityDropDown,String date_c,String time_c,String title,String desc,String contact_number,String location
 ,String quantity,String units,String numberOfPeople,String _radioValue,bool submitAnon) async {
   dynamic data={
     'user_id':await FlutterSharedPrefrence().getUserId(),
     'category': _selectCategoryDropdown,
     'first_name': first_name,
     'last_name': last_name,
     'need_priority': _selectPriorityDropDown  ,
     'till_when': date_c+" "+time_c,
     'title': title,
     'description': desc,
     'address': location,
     'lat': lat,
     'lng': lng,
     'phone_number':contact_number,
     'total_quantity': quantity,
     'units': units,
     'people': numberOfPeople,
     'delivery_option': _radioValue,
     'is_anonymous': submitAnon?"true":"false",

   };
   print(data);
    return http.post(
        ApiProvider.baseUrl+'add_request/',
      body:data
    );
  }
 static Future<http.Response> createSuggestionRequest(String lat,String lng,String _selectCategoryDropdown,String first_name,String last_name,String _selectPriorityDropDown,String title,String desc,String contact_number,String location,bool submitAnon
 ) async {
   dynamic data={
     'user_id':await FlutterSharedPrefrence().getUserId(),
     'category': _selectCategoryDropdown,
     'first_name': first_name,
     'last_name': last_name,
     'need_priority': _selectPriorityDropDown  ,
     'title': title,
     'description': desc,
     'address': location,
     'lat': lat,
     'lng': lng,
     'phone_number':contact_number,
     'is_anonymous': submitAnon?"true":"false",

   };
   print(data);
    return http.post(
        ApiProvider.baseUrl+'add_suggestion/',
      body:data
    );
  }
 static Future<http.Response> createReportRequest(String lat,String lng,String _selectCategoryDropdown,String first_name,String last_name,String _selectPriorityDropDown,String title,String desc,String contact_number,String location,bool submitAnon) async {
   dynamic data={
     'user_id':await FlutterSharedPrefrence().getUserId(),
     'category': _selectCategoryDropdown,
     'first_name': first_name,
     'last_name': last_name,
     'need_priority': _selectPriorityDropDown  ,
     'title': title,
     'description': desc,
     'address': location,
     'lat': lat,
     'lng': lng,
     'is_anonymous': submitAnon?"true":"false",
     'phone_number':contact_number,
   };
   print(data);
    return http.post(
        ApiProvider.baseUrl+'add_report/',
      body:data
    );
  }


  static Future<http.Response> verifyUser(String assessToken,String deviceName,String fcmId)  {
    dynamic data={
      'access_token': assessToken,
      'device_name': deviceName,
      'fcm_id': fcmId,
    };
     return http.post(
         ApiProvider.baseUrl+'verify_user/',
       body:data
   );
  }
}