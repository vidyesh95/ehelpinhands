import 'dart:async';

import 'package:ehelpinhands/data/FlutterSharedPrefrence.dart';
import 'package:ehelpinhands/ui/SignInScreen.dart';
import 'package:ehelpinhands/ui/home_page.dart';
import 'package:ehelpinhands/ui/util.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  static Map<int, Color> color = {
    50: Color.fromRGBO(136, 14, 79, .1),
    100: Color.fromRGBO(136, 14, 79, .2),
    200: Color.fromRGBO(136, 14, 79, .3),
    300: Color.fromRGBO(136, 14, 79, .4),
    400: Color.fromRGBO(136, 14, 79, .5),
    500: Color.fromRGBO(136, 14, 79, .6),
    600: Color.fromRGBO(136, 14, 79, .7),
    700: Color.fromRGBO(136, 14, 79, .8),
    800: Color.fromRGBO(136, 14, 79, .9),
    900: Color.fromRGBO(136, 14, 79, 1),
  };

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var primarySwatch = MaterialColor(0xFFfdd023, MyApp.color);
  String _homeScreenText = "Waiting for token...";
  bool _topicButtonsDisabled = false;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      //statusBarBrightness: Brightness.light,
      systemNavigationBarColor: Colors.amberAccent, // navigation bar color
      statusBarColor: Colors.amber, // status bar color
    ));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
          fontFamily: 'TCM',
          // _This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: primarySwatch,
          accentColor: util.primaryBrown),
//      home: MyHomePage(title: 'Flutter Demo Home Page'),

//      home: ProfileScreen(),
      home: HomePage(),
    );
  }

  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }
  }

  @override
  Future<void> initState() {
    // TODO: implement initState
    super.initState();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        _showItemDialog(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        _navigateToItemDetail(message);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      setState(() {
        _homeScreenText = "Push Messaging token: $token";
        FlutterSharedPrefrence().setFcmId(token);
      });
      print(_homeScreenText);
    });
  }

  Widget _buildDialog(BuildContext context, Item item) {
    return AlertDialog(
      content: Text("Item ${item.itemId} has been updated"),
      actions: <Widget>[
        FlatButton(
          child: const Text('CLOSE'),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        FlatButton(
          child: const Text('SHOW'),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
      ],
    );
  }

  void _showItemDialog(Map<String, dynamic> message) {
    showDialog<bool>(
      context: context,
      builder: (_) => _buildDialog(context, _itemForMessage(message)),
    ).then((bool shouldNavigate) {
      if (shouldNavigate == true) {
        _navigateToItemDetail(message);
      }
    });
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final Item item = _itemForMessage(message);
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
}

class TEMP extends StatefulWidget {
  @override
  _TEMPState createState() => _TEMPState();
}

class _TEMPState extends State<TEMP> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

final Map<String, Item> _items = <String, Item>{};

Item _itemForMessage(Map<String, dynamic> message) {
  final dynamic data = message['data'] ?? message;
  final String itemId = data['id'];
  final Item item = _items.putIfAbsent(itemId, () => Item(itemId: itemId))
    ..status = data['status'];
  return item;
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    checkUpdate().then((value) async {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      String version = packageInfo.version;
      var buildNumber = int.parse(packageInfo.buildNumber);
      print("version --- $buildNumber");
      var serverVersion = value.getInt('version_code');
      var serverLink = value.getString('link');
      var title = value.getString('title');
      var message = value.getString('message');
      if (buildNumber < serverVersion) {
        print("show dialog");
        _neverSatisfied(title, message, serverLink, context);
      } else {
        startTime();
      }
    });
  }

  Future<void> _neverSatisfied(
      String title, String message, String url, BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title,
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  message,
                  style: TextStyle(fontSize: 18),
                ),
//                Text('You\’re like me. I’m never satisfied.'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              color: util.primaryBrown,
              child: Text(
                title,
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
              onPressed: () {
                _launchURL(url);
//                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _launchURL(String inUrl) async {
    var url = inUrl;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<RemoteConfig> checkUpdate() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    final RemoteConfig remoteConfig = await RemoteConfig.instance;

    try {
      final defaults = <String, dynamic>{
        'version_code': packageInfo.buildNumber,
        'link':
            'https://play.google.com/store/apps/details?id=com.anirdesh.ehelpinhands',
        'title': 'Update',
        'message': 'Please Update the app'
      };
      await remoteConfig.setDefaults(defaults);
      // Using default duration to force fetching from remote server.
      remoteConfig.setConfigSettings(RemoteConfigSettings(debugMode: true));

      await remoteConfig.fetch(expiration: const Duration(seconds: 0));
      await remoteConfig.activateFetched();
      print('welcome message: ' + remoteConfig.getString('version_code'));
    } on FetchThrottledException catch (exception) {
      // Fetch throttled.
      print(exception);
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }

//    await remoteConfig.fetch(expiration: const Duration(hours: 5));
//    await remoteConfig.activateFetched();
//    print('welcome message: ' + remoteConfig.getString('version_code'));
    return remoteConfig;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: util.primaryYello,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                "assets/images/ehh_logo_d.png",
                fit: BoxFit.cover,
                height: 150,
                width: 150,
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                "eHelpinHands",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 45,
                    color: util.primaryBrown,
                    fontFamily: "TCM",
                    decoration: TextDecoration.none),
              ),
              SizedBox(
                height: 120,
              ),
              Text(
                "Available at Your Service...",
                style: TextStyle(
                    fontSize: 20,
                    color: util.primaryBrown,
                    fontFamily: "TCM",
                    decoration: TextDecoration.none),
              )
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
                padding: EdgeInsets.only(left: 40, right: 40, bottom: 10),
                child: Text(
                  "Copyright © eHelpinHands.com All Rights Reserved. Powered by: Infornation Techserve Pvt. Ltd. All the Brands, Trademarks & Copyrights are the Property of their Respective Holders.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 8,
                      color: util.primaryBrown,
                      fontFamily: "TCM",
                      decoration: TextDecoration.none),
                )),
          )
        ],
      ),
    );
  }

  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
//    Navigator.of(context).popUntil((route) => false);

    Future<String> id = FlutterSharedPrefrence().getUserId();
    id.then((value) {
      if (value != null)
        Navigator.of(context).pushAndRemoveUntil(
          new MaterialPageRoute(builder: (context) => new HomePage()),
          (Route<dynamic> route) => false,
        );
      else
        Navigator.of(context).pushAndRemoveUntil(
          new MaterialPageRoute(builder: (context) => new SignInScreen()),
          (Route<dynamic> route) => false,
        );
    });
  }
}

class Item {
  Item({this.itemId});

  final String itemId;

  StreamController<Item> _controller = StreamController<Item>.broadcast();

  Stream<Item> get onChanged => _controller.stream;

  String _status;

  String get status => _status;

  set status(String value) {
    _status = value;
    _controller.add(this);
  }

  static final Map<String, Route<void>> routes = <String, Route<void>>{};

  Route<void> get route {
    final String routeName = '/detail/$itemId';
    return routes.putIfAbsent(
      routeName,
      () => MaterialPageRoute<void>(
        settings: RouteSettings(name: routeName),
        builder: (BuildContext context) => DetailPage(itemId),
      ),
    );
  }
}

class DetailPage extends StatefulWidget {
  DetailPage(this.itemId);

  final String itemId;

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  Item _item;
  StreamSubscription<Item> _subscription;

  @override
  void initState() {
    super.initState();
    _item = _items[widget.itemId];
    _subscription = _item.onChanged.listen((Item item) {
      if (!mounted) {
        _subscription.cancel();
      } else {
        setState(() {
          _item = item;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Item ${_item.itemId}"),
      ),
      body: Material(
        child: Center(child: Text("Item status: ${_item.status}")),
      ),
    );
  }
}
