import 'package:ehelpinhands/models/NeedRequestModel.dart';
import 'package:ehelpinhands/models/UserResponse.dart';
import 'package:ehelpinhands/networking/ApiProvider.dart';

class UserRepository{
  ApiProvider _provider = ApiProvider();


  Future<UserResponse> signUp(dynamic data) async {
    final response = await _provider.post("signup/",data);
    return UserResponse.fromJson(response);
  }
  Future<UserResponse> updateProfile(dynamic data) async {
    final response = await _provider.post("update_profile/",data);
    return UserResponse.fromJson(response);
  }
  Future<UserResponse> signIn(dynamic data) async {
    final response = await _provider.post("signin/",data);
    return UserResponse.fromJson(response);
  }
}