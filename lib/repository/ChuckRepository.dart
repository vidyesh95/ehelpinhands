import 'package:ehelpinhands/models/NeedRequestModel.dart';
import 'package:ehelpinhands/models/ReportRequestModel.dart';
import 'package:ehelpinhands/models/SuggestionRequestModel.dart';
import 'package:ehelpinhands/models/checkResponse.dart';
import 'package:ehelpinhands/networking/ApiProvider.dart';

class ChuckRepository {
  ApiProvider _provider = ApiProvider();

  Future<chuckResponse> fetchChuckJoke(String category) async {
    final response = await _provider.get("jokes/random?category=" + category);
    return chuckResponse.fromJson(response);
  }
  Future<NeedRequestModel> fetchNeedsList(String user_id,String category,String lat,String lng,String cat2) async {
   dynamic data={
//      'user_id':user_id,
     'category':category,
     'lat':lat,
     'lng':lng,
     'category2':cat2
    };
    final response = await _provider.post("list_need_requests/",data);
    print("RESPONSE => $response");
    return NeedRequestModel.fromJson(response);
  }
  Future<NeedRequestModel> fetchMyNeedsList(String user_id) async {
   dynamic data={
      'user_id':user_id,
//     'category':category,
//     'lat':lat,
//     'lng':lng,
//     'category2':cat2
    };
    final response = await _provider.post("list_my_need_requests/",data);
    print("RESPONSE => $response");
    return NeedRequestModel.fromJson(response);
  }

  Future<NeedRequestModel> fetchMyNeedList(String user_id) async {
   dynamic data={
      'user_id':user_id,
//     'category':category,
//     'lat':lat,
//     'lng':lng,
//     'category2':cat2
    };
    final response = await _provider.post("list_my_need_requests/",data);
    print("RESPONSE => $response");
    return NeedRequestModel.fromJson(response);
  }

  Future<SuggestionRequestModel> fetchAllSuggestionList(String user_id,String category,String lat,String lng,String cat2) async {
    dynamic data={
//      'user_id':user_id,
      'category':category,
      'lat':lat,
      'lng':lng,
      'category2':cat2
    };
    final response = await _provider.post("list_suggestion/",data);
    print("RESPONSE => $response");
    return SuggestionRequestModel.fromJson(response);
  }

  Future<SuggestionRequestModel> fetchMySuggestionList(String user_id) async {
    dynamic data={
      'user_id':user_id,
//     'category':category,
//     'lat':lat,
//     'lng':lng,
//     'category2':cat2
    };
    final response = await _provider.post("list_my_suggestion/",data);
    print("RESPONSE => $response");
    return SuggestionRequestModel.fromJson(response);
  }
  Future<ReportRequestModel> fetchAllReportList(String user_id,String category,String lat,String lng,String cat2) async {
    dynamic data={
//      'user_id':user_id,
      'category':category,
      'lat':lat,
      'lng':lng,
      'category2':cat2
    };
    final response = await _provider.post("list_reports/",data);
    print("RESPONSE => $response");
    return ReportRequestModel.fromJson(response);
  }

  Future<ReportRequestModel> fetchMyReportList(String user_id) async {
    dynamic data={
      'user_id':user_id,
//     'category':category,
//     'lat':lat,
//     'lng':lng,
//     'category2':cat2
    };
    final response = await _provider.post("list_my_reports/",data);
    print("RESPONSE => $response");
    return ReportRequestModel.fromJson(response);
  }
}