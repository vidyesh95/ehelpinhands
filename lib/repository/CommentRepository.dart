import 'package:ehelpinhands/models/NeedCommentsModel.dart';
import 'package:ehelpinhands/models/NeedRequestModel.dart';
import 'package:ehelpinhands/networking/ApiProvider.dart';

class CommentRepository{
  ApiProvider _provider = ApiProvider();

  Future<NeedCommentsModel> fetchCommentList(String user_id,String need_id,String type) async {
    dynamic data={
      'user_id':user_id,
      'need_id':need_id,
      'type':type
    };
    final response = await _provider.post("list_comments/",data);
    return NeedCommentsModel.fromJson(response);
  }
  Future<NeedCommentsModel> postComments(String message,String user_id,String need_id) async {
    dynamic data={
      'message':message,
      'user_id':user_id,
      'need_id':need_id
    };
    final response = await _provider.post("add_comment/",data);
    return NeedCommentsModel.fromJson(response);
  }

}