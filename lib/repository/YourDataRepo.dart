import 'package:ehelpinhands/models/NeedCommentsModel.dart';
import 'package:ehelpinhands/models/NeedRequestModel.dart';
import 'package:ehelpinhands/networking/ApiProvider.dart';

class YourDataRepo{
  ApiProvider _provider = ApiProvider();


  Future<NeedRequestModel> fetchYourNeedsList(String user_id,) async {
    dynamic data={
//      'user_id':user_id,
//      'category':category,
//      'lat':lat,
//      'lng':lng,
//      'category2':cat2
    };
    final response = await _provider.post("list_need_requests/",data);
    print("RESPONSE => $response");
    return NeedRequestModel.fromJson(response);
  }

  Future<NeedRequestModel> fetchYourSuggestionList(String user_id) async {
    dynamic data={
      'user_id':user_id,
    };
    final response = await _provider.post("list_need_requests/",data);
    print("RESPONSE => $response");
    return NeedRequestModel.fromJson(response);
  }

  Future<NeedRequestModel> fetchYourReportsList(String user_id) async {
    dynamic data={
      'user_id':user_id,
    };
    final response = await _provider.post("list_need_requests/",data);
    print("RESPONSE => $response");
    return NeedRequestModel.fromJson(response);
  }
}