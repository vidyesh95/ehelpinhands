class NeedData {
  int id;
  String firstName;
  String lastName;
  String needPriority;
  String title;
  String description;
  String lat;
  String lng;
  String phoneNumber;
  String tillWhen;
  String totalQuantity;
  String units;
  String people;
  String deliveryOption;
  String createdAt;
  String updatedAt;
  String category;
  bool hasComments;
  String address;
  bool isAnonymous;

  NeedData(
      {this.id,
        this.firstName,
        this.lastName,
        this.needPriority,
        this.title,
        this.description,
        this.lat,
        this.lng,
        this.phoneNumber,
        this.tillWhen,
        this.totalQuantity,
        this.units,
        this.people,
        this.deliveryOption,
        this.createdAt,
        this.updatedAt,
        this.category,
        this.hasComments,
        this.address,
        this.isAnonymous
      });

  NeedData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    needPriority = json['need_priority'];
    title = json['title'];
    description = json['description'];
    lat = json['lat'];
    lng = json['lng'];
    phoneNumber = json['phone_number'];
    tillWhen = json['till_when'];
    totalQuantity = json['total_quantity'];
    units = json['units'];
    people = json['people'];
    deliveryOption = json['delivery_option'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    category = json['category'];
    hasComments = json['has_comments'];
    address = json['address'];
    isAnonymous = json['is_anonymous'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['need_priority'] = this.needPriority;
    data['title'] = this.title;
    data['description'] = this.description;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['phone_number'] = this.phoneNumber;
    data['till_when'] = this.tillWhen;
    data['total_quantity'] = this.totalQuantity;
    data['units'] = this.units;
    data['people'] = this.people;
    data['delivery_option'] = this.deliveryOption;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['category'] = this.category;
    data['has_comments'] = this.hasComments;
    data['address'] = this.address;
    data['is_anonymous'] = this.isAnonymous;
    return data;
  }
}