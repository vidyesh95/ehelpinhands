class CommentData {
  String firstName;
  String lastName;
  String message;
  String createdAt;
  String updatedAt;
  int commentById;

  CommentData(
      {this.firstName,
        this.lastName,
        this.message,
        this.createdAt,
        this.updatedAt,
        this.commentById});

  CommentData.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    lastName = json['last_name'];
    message = json['message'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    commentById = json['comment_by_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['message'] = this.message;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['comment_by_id'] = this.commentById;
    return data;
  }
}