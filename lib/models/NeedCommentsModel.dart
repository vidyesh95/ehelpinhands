import 'package:ehelpinhands/models/CommentData.dart';

class NeedCommentsModel {
  List<CommentData> data;
  bool status;
  String message;

  NeedCommentsModel({this.data, this.status, this.message});

  NeedCommentsModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<CommentData>();
      json['data'].forEach((v) {
        data.add(new CommentData.fromJson(v));
      });
    }
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class Data {
  String firstName;
  String lastName;
  String message;
  String createdAt;
  String updatedAt;

  Data(
      {this.firstName,
        this.lastName,
        this.message,
        this.createdAt,
        this.updatedAt});

  Data.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    lastName = json['last_name'];
    message = json['message'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['message'] = this.message;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}