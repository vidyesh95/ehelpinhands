class UserResponse {
  bool status;
  String message;
  Data data;

  UserResponse({this.status, this.message, this.data});

  UserResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int id;
  String firstName;
  String lastName;
  String email;
  String phoneNumber;
  String deviceName;
  String lat;
  String lng;
  bool isBlocked;
  String dob;
  String gender;

  Data(
      {this.id,
        this.firstName,
        this.lastName,
        this.email,
        this.phoneNumber,
        this.deviceName,
        this.lat,
        this.lng,
        this.isBlocked,
        this.dob,
        this.gender});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    phoneNumber = json['phone_number'];
    deviceName = json['device_name'];
    lat = json['lat'];
    lng = json['lng'];
    isBlocked = json['is_blocked'];
    dob = json['dob'];
    gender = json['gender'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['phone_number'] = this.phoneNumber;
    data['device_name'] = this.deviceName;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['is_blocked'] = this.isBlocked;
    data['dob'] = this.dob;
    data['gender'] = this.gender;
    return data;
  }
}