import 'package:ehelpinhands/models/NeedData.dart';

class SuggestionRequestModel {
  bool status;
  String message;
  List<NeedData> data;

  SuggestionRequestModel({this.status, this.message, this.data});

  SuggestionRequestModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<NeedData>();
      json['data'].forEach((v) {
        data.add(new NeedData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
